package testcases;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.junit.Test;

import domain.Supplier;
import testclasses.testMethods;


public class TestCase {

	@Test
	public void testDoLogin() {
		int optionFirst = JOptionPane.OK_OPTION;
		boolean correctFirst = true;
		int optionSecond = JOptionPane.OK_OPTION;
		boolean correctSecond = false;
		int optionThird = JOptionPane.CLOSED_OPTION;
		boolean correctThird = true;
		
		testMethods method = new testMethods();
		
		assertTrue("doLogin met option == ok_option en correct = true", method.doLogin(optionFirst, correctFirst));
		assertFalse("doLogin met option == ok_option en correct = false", method.doLogin(optionSecond, correctSecond));
		assertFalse("doLogin met option != ok_option en correct = true", method.doLogin(optionThird, correctThird));
	}
	
	
	@Test
	public void testItemChanged() {
		int index1 = 0;
		int index2 = 1;
		int index3 = 2;
		int index4 = 3;
		int index5 = 4;
		int index6 = 5;
		int index7 = 6;
		int index8 = 7;
		int index9 = 8;
		int index10 = 9;
		int index11 = 15;
				
		testMethods method = new testMethods();
		
		assertEquals("itemchanged met index = 0", "ORDER BY ingredient_id ASC", method.itemStateChanged(index1));
		assertEquals("itemchanged met index = 1", "ORDER BY ingredient_id DESC", method.itemStateChanged(index2));
		assertEquals("itemchanged met index = 2", "ORDER BY ingredient_name ASC", method.itemStateChanged(index3));
		assertEquals("itemchanged met index = 3", "ORDER BY ingredient_name DESC", method.itemStateChanged(index4));
		assertEquals("itemchanged met index = 4", "ORDER BY stock ASC", method.itemStateChanged(index5));
		assertEquals("itemchanged met index = 5", "ORDER BY stock DESC", method.itemStateChanged(index6));
		assertEquals("itemchanged met index = 6", "ORDER BY min ASC", method.itemStateChanged(index7));
		assertEquals("itemchanged met index = 7", "ORDER BY min DESC", method.itemStateChanged(index8));
		assertEquals("itemchanged met index = 8", "ORDER BY stock - min ASC", method.itemStateChanged(index9));
		assertEquals("itemchanged met index = 9", "ORDER BY stock - min DESC", method.itemStateChanged(index10));
		assertEquals("itemchanged met index = 15", "ORDER BY stock - min DESC", method.itemStateChanged(index11));
	}
	
	@Test
	public void testActionPerformedThreeLoops() {
		int getRowCount = 3;
		ArrayList<Supplier> getSuppliers = new ArrayList<Supplier>();
		int supplierId = 1;
		String name = "Piet";
		String nameFirst = "Jan";
		String nameSecond = "Piet";
		String address = "Lovensdijkstraat 10";
		String zipcode = "3329AD";
		String phone = "123456789";
		String email = "email@email.com";
		BigDecimal shippingCost = new BigDecimal(25);
		getSuppliers.add(new Supplier(1, nameFirst, address, zipcode, phone, email, shippingCost));
		getSuppliers.add(new Supplier(2, nameSecond, address, zipcode, phone, email, shippingCost));
		ArrayList<Object> expected = new ArrayList<Object>();
		expected.add(3);
		expected.add(6);
		ArrayList<Boolean> expectedFirstIf = new ArrayList<Boolean>();
		expectedFirstIf.add(true);
		expectedFirstIf.add(false);
		expectedFirstIf.add(true);
		expectedFirstIf.add(false);
		expectedFirstIf.add(true);
		expectedFirstIf.add(false);
		ArrayList<Boolean> expectedSecondIf = new ArrayList<Boolean>();
		expectedSecondIf.add(false);
		expectedSecondIf.add(true);
		expectedSecondIf.add(false);
		expectedSecondIf.add(true);
		expectedSecondIf.add(false);
		expectedSecondIf.add(true);
		expected.add(expectedFirstIf);
		expected.add(expectedSecondIf);
		testMethods method = new testMethods();
				
		assertEquals("actionperformed met drie loops", expected, method.actionPerformed(getRowCount, supplierId, name, getSuppliers));
		
	}
	
	@Test
	public void testActionPerformedZeroLoops() {
		int getRowCount = 0;
		ArrayList<Supplier> getSuppliers = new ArrayList<Supplier>();
		int supplierId = 1;
		String name = "Piet";
		String nameFirst = "Jan";
		String address = "Lovensdijkstraat 10";
		String zipcode = "3329AD";
		String phone = "123456789";
		String email = "email@email.com";
		BigDecimal shippingCost = new BigDecimal(25);
		getSuppliers.add(new Supplier(1, nameFirst, address, zipcode, phone, email, shippingCost));
		ArrayList<Object> expected = new ArrayList<Object>();
		expected.add(0);
		expected.add(0);
		ArrayList<Boolean> expectedFirstIf = new ArrayList<Boolean>();
		ArrayList<Boolean> expectedSecondIf = new ArrayList<Boolean>();
		expected.add(expectedFirstIf);
		expected.add(expectedSecondIf);
		testMethods method = new testMethods();
				
		assertEquals("actionperformed met nul loops", expected, method.actionPerformed(getRowCount, supplierId, name, getSuppliers));
		
	}
	
	@Test
	public void testActionPerformedOneLoop() {
		int getRowCount = 1;
		ArrayList<Supplier> getSuppliers = new ArrayList<Supplier>();
		int supplierId = 1;
		String name = "Piet";
		String nameFirst = "Piet";
		String address = "Lovensdijkstraat 10";
		String zipcode = "3329AD";
		String phone = "123456789";
		String email = "email@email.com";
		BigDecimal shippingCost = new BigDecimal(25);
		getSuppliers.add(new Supplier(1, nameFirst, address, zipcode, phone, email, shippingCost));
		ArrayList<Object> expected = new ArrayList<Object>();
		expected.add(1);
		expected.add(1);
		ArrayList<Boolean> expectedFirstIf = new ArrayList<Boolean>();
		expectedFirstIf.add(true);
		ArrayList<Boolean> expectedSecondIf = new ArrayList<Boolean>();
		expectedSecondIf.add(true);
		expected.add(expectedFirstIf);
		expected.add(expectedSecondIf);
		testMethods method = new testMethods();
				
		assertEquals("actionperformed met één loop", expected, method.actionPerformed(getRowCount, supplierId, name, getSuppliers));
		
	}
}