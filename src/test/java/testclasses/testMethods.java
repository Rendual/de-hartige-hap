package testclasses;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import domain.Supplier;

public class testMethods {

	public boolean doLogin(int option, boolean correct) {
		if (option == JOptionPane.OK_OPTION) {
			try {
				if (correct) {
					return true;
				} else {
					return false;
				}
			} catch (NumberFormatException e) {
				return false;
			}
		}
		return false;
	}

	public String itemStateChanged(int index) {
		String order = "";
		switch (index) {
		case 0:
			order = "ORDER BY ingredient_id ASC";
			break;
		case 1:
			order = "ORDER BY ingredient_id DESC";
			break;
		case 2:
			order = "ORDER BY ingredient_name ASC";
			break;
		case 3:
			order = "ORDER BY ingredient_name DESC";
			break;
		case 4:
			order = "ORDER BY stock ASC";
			break;
		case 5:
			order = "ORDER BY stock DESC";
			break;
		case 6:
			order = "ORDER BY min ASC";
			break;
		case 7:
			order = "ORDER BY min DESC";
			break;
		case 8:
			order = "ORDER BY stock - min ASC";
			break;
		case 9:
			order = "ORDER BY stock - min DESC";
			break;
		default:
			order = "ORDER BY stock - min DESC";
			break;
		}
		return order;

	}
	
	public ArrayList<Object> actionPerformed(int getRowCount, int id, String name, ArrayList<Supplier> getSuppliers) {
		ArrayList<Object> retValue = new ArrayList<Object>();
		int checkMainLoop = 0;
		int checkSecondLoop = 0;
		ArrayList<Boolean> checkFirstIf = new ArrayList<Boolean>();
		ArrayList<Boolean> checkSecondIf = new ArrayList<Boolean>();
		for (int i = 0; i < getRowCount; i++) {
			for (Supplier supplier : getSuppliers) {
				boolean checkSecondIfSingle = true;
				boolean checkFirstIfSingle = false;
				if (supplier.getId() == id) {
					if (!supplier.getName().equals(name)) {
						checkSecondIfSingle = false;
					}
					checkFirstIfSingle = true;
				}
				checkSecondLoop++;
				checkFirstIf.add(checkFirstIfSingle);
				checkSecondIf.add(checkSecondIfSingle);
			}
			checkMainLoop++;
		}
		retValue.add(checkMainLoop);
		retValue.add(checkSecondLoop);
		retValue.add(checkFirstIf);
		retValue.add(checkSecondIf);
		return retValue;
	}


}