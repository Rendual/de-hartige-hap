package presentation;

import java.awt.Color;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.MatteBorder;

public class LoadSmall extends JDialog {

	private static final long serialVersionUID = 1L;
	
	//The GUI component
	private JPanel contentPane;
	private JLabel lblLoad;

    /**
     * Initializes the fields of this class and creates the GUI
     * @param parent the JFrame this dialog is displayed on
     * @param message the loading message
     */
	public LoadSmall(JFrame parent, String message) {
		super(parent, "", false);
		
		//set the frame attributes
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setSize(154, 36);
		setLocationRelativeTo(parent);
		setUndecorated(true);
		setBackground(new Color(1.0f, 1.0f, 1.0f, 0.5f));
		
		//create the content pane and set the attributes
		contentPane = new JPanel();
		contentPane.setBackground(new Color(240, 240, 240));
		contentPane.setBorder(new MatteBorder(1, 1, 1, 1,
				(Color) Color.LIGHT_GRAY));
		contentPane.setLayout(null);
		setContentPane(contentPane);

		//create the loading label and set the attributes
		lblLoad = new JLabel(message);
		lblLoad.setHorizontalAlignment(SwingConstants.CENTER);
		lblLoad.setBounds(10, 11, 134, 14);
		contentPane.add(lblLoad);

		setVisible(true);
	}
}