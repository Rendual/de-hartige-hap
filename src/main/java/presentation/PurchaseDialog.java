package presentation;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import businesslogic.ProductManager;
import domain.OrderItem;
import domain.Product;

public class PurchaseDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	//The GUI components
	private JButton btnOrder;
	private JButton btnSearch;
	private JButton btnShowAll;
	private JComboBox<String> cbOrder;
	private JPanel contentPane;
	private JLabel lblArrow;
	private JLabel lblLegend1;
	private JLabel lblLegend2;
	private JLabel lblLegend3;
	private JLabel lblLegend4;
	private JLabel lblPurchaseList;
	private JLabel lblSort;
	private JLabel lblSummary;
	private JScrollPane scrollPane1;
	private JScrollPane scrollPane2;
	private JSeparator separator1;
	private JSeparator separator2;
	private JTable table;
	private JTable table3;
	private JTextField tfSearch;
	
	//The search order that is used to display stock
	private String order = "ORDER BY stock - min ASC";
	
    /**
     * Initializes the fields of this class and creates the GUI
     * @param gui the manage GUI
     * @param parent the JFrame this dialog is displayed on
     * @param manager the product manager
     */
	public PurchaseDialog(final ManageGUI gui, final JFrame parent, final ProductManager manager) {
		super(parent, "Bestellen", false);

		//create the loading screen
		LoadSmall load = new LoadSmall(parent, "Aan het laden...");

		//set the frame attributes
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setSize(608, 452);
		setLocationRelativeTo(parent);

		//create the content pane and set the attributes
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);

		//create the search textfield and set the attributes
		tfSearch = new JTextField();
		tfSearch.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					if (!tfSearch.getText().equals("")) {
						btnSearch.doClick();
					} else {
						btnShowAll.doClick();
					}
					tfSearch.selectAll();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {

			}

			@Override
			public void keyTyped(KeyEvent e) {

			}
		});
		tfSearch.setBounds(12, 45, 202, 23);
		contentPane.add(tfSearch);
		tfSearch.setColumns(10);

		//create the search button and set the attributes
		btnSearch = new JButton("Zoeken");
		btnSearch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				refresh(manager);
			}
		});
		btnSearch.setBounds(224, 45, 97, 25);
		contentPane.add(btnSearch);

		//create the first scrollpane and set the attributes
		scrollPane1 = new JScrollPane();
		scrollPane1.setBounds(12, 123, 309, 250);
		contentPane.add(scrollPane1);

		//create the stock table and set the attrivutes
		table = new JTable() {

			private static final long serialVersionUID = 1L;

			@Override
			public Component prepareRenderer(TableCellRenderer renderer,
					int row, int column) {
				Component c = super.prepareRenderer(renderer, row, column);
				if ((Integer) table.getValueAt(
						row,
						table.convertColumnIndexToView(table.getColumn(
								"Voorraad").getModelIndex())) <= (Integer) table
						.getValueAt(
								row,
								table.convertColumnIndexToView(table.getColumn(
										"Min").getModelIndex()))) {
					c.setBackground(Color.RED);
					c.setForeground(Color.WHITE);
				} else if (((Integer) table.getValueAt(
						row,
						table.convertColumnIndexToView(table.getColumn(
								"Voorraad").getModelIndex())) - (Integer) table
						.getValueAt(
								row,
								table.convertColumnIndexToView(table.getColumn(
										"Min").getModelIndex()))) < 6) {
					c.setBackground(new Color(255, 140, 0));
					c.setForeground(Color.BLACK);
				} else {
					c.setBackground(Color.WHITE);
					c.setForeground(Color.BLACK);
				}
				return c;
			}
		};
		scrollPane1.setViewportView(table);
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] {
				"ID", "Naam", "Voorraad", "Min" }) {

			private static final long serialVersionUID = 1L;

			boolean[] columnEditables = new boolean[] { false, false, false,
					false };

			@Override
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});

		//create the sort label and set the attributes
		lblSort = new JLabel("Sorteer op:");
		lblSort.setBounds(12, 87, 61, 16);
		contentPane.add(lblSort);

		//create the order combobox and set the attributes
		cbOrder = new JComboBox<String>();
		cbOrder.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				switch (cbOrder.getSelectedIndex()) {
				case 0:
					order = "ORDER BY ingredient_id ASC";
					break;
				case 1:
					order = "ORDER BY ingredient_id DESC";
					break;
				case 2:
					order = "ORDER BY ingredient_name ASC";
					break;
				case 3:
					order = "ORDER BY ingredient_name DESC";
					break;
				case 4:
					order = "ORDER BY stock ASC";
					break;
				case 5:
					order = "ORDER BY stock DESC";
					break;
				case 6:
					order = "ORDER BY min ASC";
					break;
				case 7:
					order = "ORDER BY min DESC";
					break;
				case 8:
					order = "ORDER BY stock - min ASC";
					break;
				case 9:
					order = "ORDER BY stock - min DESC";
					break;
				default:
					order = "ORDER BY stock - min DESC";
					break;
				}
				refresh(manager);
			}
		});
		cbOrder.setModel(new DefaultComboBoxModel<String>(new String[] {
				"ID - oplopend", "ID - aflopend", "Naam - a-z", "Naam - z-a",
				"Voorraad - oplopend", "Voorraad - aflopend", "Min - oplopend",
				"Min - aflopend", "(Vrd-Min) - oplopend", "(Vrd-Min) - aflopend" }));
		cbOrder.setSelectedIndex(8);
		cbOrder.setBounds(75, 85, 139, 23);
		contentPane.add(cbOrder);

		//create the first legend label and set the attributes
		lblLegend1 = new JLabel("");
		lblLegend1.setBackground(Color.RED);
		lblLegend1.setOpaque(true);
		lblLegend1.setBounds(13, 387, 15, 15);
		contentPane.add(lblLegend1);

		//create the second legend label and set the attributes
		lblLegend2 = new JLabel("Voorraad <= min.");
		lblLegend2.setBounds(35, 387, 104, 16);
		contentPane.add(lblLegend2);

		//create the third legend label and set the attributes
		lblLegend3 = new JLabel("");
		lblLegend3.setOpaque(true);
		lblLegend3.setBackground(new Color(255, 140, 0));
		lblLegend3.setBounds(139, 387, 15, 15);
		contentPane.add(lblLegend3);

		//create the fourth legend label and set the attributes
		lblLegend4 = new JLabel("Voorraad < 6 boven min.");
		lblLegend4.setBounds(161, 387, 144, 16);
		contentPane.add(lblLegend4);

		btnShowAll = new JButton("Toon alles");
		btnShowAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tfSearch.setText("");
				refresh(manager);
			}
		});
		btnShowAll.setBounds(224, 84, 97, 25);
		contentPane.add(btnShowAll);

		//create the order button and set the attributes
		btnOrder = new JButton("Bestellen");
		btnOrder.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (table3.getRowCount() > 0) {
					boolean login = new Login().doLogin(PurchaseDialog.this,
							manager, false);
					if (login) {
						//create the loading screen
						LoadSmall load = new LoadSmall(parent,
								"Bestellijst opstellen...");
						List<OrderItem> items = new ArrayList<OrderItem>();
						for (int i = 0; i < table3.getRowCount(); i++) {
							int id = (Integer) table3.getValueAt(i, table3
									.convertColumnIndexToView(table3.getColumn(
											"ID").getModelIndex()));
							String name = (String) table3.getValueAt(i, table3
									.convertColumnIndexToView(table3.getColumn(
											"Naam").getModelIndex()));
							items.add(new OrderItem(
									id,
									name,
									manager.getSupplierFromProd(id),
									1,
									manager.getPriceShippCost(id, manager
											.getSupplierFromProd(id).getId())[0]));
						}
						load.dispose();
						//open the order dialog
						new OrderDialog(gui, parent, manager, items);
						dispose();
					}
				} else {
					JOptionPane
							.showMessageDialog(
									PurchaseDialog.this,
									"U moet eerst producten aan uw bestellijst toevoegen om verder te kunnen!\nU kunt een product toevoegen door erop te klikken",
									"Info", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		btnOrder.setBounds(380, 84, 210, 25);
		contentPane.add(btnOrder);

		//create the second scrollpane and set the attributes
		scrollPane2 = new JScrollPane();
		scrollPane2.setBounds(380, 123, 210, 250);
		contentPane.add(scrollPane2);

		//create the third table and set the attributes
		table3 = new JTable();
		table3.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent evnt) {
				DefaultTableModel model = (DefaultTableModel) table3.getModel();
				model.removeRow(table3.getSelectedRow());
			}
		});
		table3.setModel(new DefaultTableModel(new Object[][] {}, new String[] {
				"ID", "Naam", "Leverancier" }));
		table3.getColumnModel().getColumn(0).setPreferredWidth(50);
		scrollPane2.setViewportView(table3);

		//create the arrow label and set the attributes
		lblArrow = new JLabel("");
		lblArrow.setBounds(331, 224, 41, 41);
		
		//get the arrow image and resize it to fit the arrow label
		Image image = new ImageIcon(getClass().getResource("/Images/arrow.png"))
				.getImage();
		Image newimg = image.getScaledInstance(lblArrow.getWidth(),
				lblArrow.getHeight(), java.awt.Image.SCALE_SMOOTH);
		ImageIcon imageIcon = new ImageIcon(newimg);
		
		//set the arrow image as icon for the arrow label
		lblArrow.setIcon(imageIcon);
		contentPane.add(lblArrow);

		//create the aummary label and set the attributes
		lblSummary = new JLabel("Overzicht");
		lblSummary.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSummary.setBounds(12, 12, 58, 17);
		contentPane.add(lblSummary);

		//create the first separator and set the attributes
		separator1 = new JSeparator();
		separator1.setBounds(12, 36, 309, 2);
		contentPane.add(separator1);

		//create the second separator and set the attributes
		separator2 = new JSeparator();
		separator2.setBounds(377, 36, 210, 2);
		contentPane.add(separator2);

		//create the purchase list label and set the attributes
		lblPurchaseList = new JLabel("Bestellijst");
		lblPurchaseList.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPurchaseList.setBounds(380, 12, 58, 17);
		contentPane.add(lblPurchaseList);

		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setHeaderValue("ID");
		table.getColumnModel().getColumn(2).setResizable(false);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent evnt) {
				LoadSmall load = new LoadSmall(parent, "Aan het laden...");
				int id = (Integer) table.getValueAt(table.getSelectedRow(),
						table.convertColumnIndexToView(table.getColumn("ID")
								.getModelIndex()));
				String name = (String) table.getValueAt(table.getSelectedRow(),
						table.convertColumnIndexToView(table.getColumn("Naam")
								.getModelIndex()));
				boolean isAlready = false;
				for (int i = 0; i < table3.getRowCount(); i++) {
					if ((Integer) table3.getValueAt(
							i,
							table3.convertColumnIndexToView(table3.getColumn(
									"ID").getModelIndex())) == id) {
						isAlready = true;
						JOptionPane
								.showMessageDialog(
										PurchaseDialog.this,
										"U heeft dit product al toegevoegd aan uw bestellijst!",
										"Info", JOptionPane.INFORMATION_MESSAGE);
						int focRow = getRowByValue(
								(DefaultTableModel) table3.getModel(), id);
						table3.setRowSelectionInterval(focRow, focRow);
					}
				}
				if (!isAlready) {
					DefaultTableModel model = (DefaultTableModel) table3
							.getModel();
					Object[] rowData = { id, name,
							manager.getSupplierFromProd(id).getName() };
					model.addRow(rowData);
				}
				load.dispose();
			}
		});
		load.dispose();
		
		setVisible(true);
		
		if (table.getRowCount() < 1) {
			btnSearch.doClick();
		}
		tfSearch.requestFocus();
		
		//create a refresh timer
		Timer timer = new Timer();
		timer.schedule(new Refresh(manager), 5000, 5000);
	}

	/**
	 * Gets the row for a value
	 * @param model the table model
	 * @param value the value
	 * @return the row as an int
	 */
	private int getRowByValue(TableModel model, int value) {
		for (int i = model.getRowCount() - 1; i >= 0; --i) {
			if ((Integer) model.getValueAt(i, 0) == value) {
				return i;
			}
		}
		return 0;
	}

	/**
	 * Fills the stock table with products and updates the table
	 * @param productList the list of products to show
	 */
	private void show(List<Product> productList) {
		int pos = scrollPane1.getVerticalScrollBar().getValue();
		try {
			DefaultTableModel dtm = (DefaultTableModel) table.getModel();
			dtm.setRowCount(0);
			for (Product product : productList) {
				Object[] list = { product.getId(), product.getTitle(),
						product.getStock(), product.getMin() };
				dtm.addRow(list);
			}
			table.setModel(dtm);
		} finally {
			scrollPane1.getVerticalScrollBar().setValue(pos);
		}
	}
	
	/**
	 * Refreshes the stock table by retrieving the product list from the manager, 
	 * this method takes into account the current search text and sort order
	 */
	private void refresh(ProductManager manager) {
		List<Product> productlist = (ArrayList<Product>) manager
				.giveProductList(tfSearch.getText(), order);
		show((ArrayList<Product>) productlist);
	}
	
	private class Refresh extends TimerTask {
		
		//The product manager
		private ProductManager manager;
	
	    /**
	     * Initializes the fields of this class
	     * @param manager the product manager
	     */
		public Refresh(ProductManager manager) {
			this.manager = manager;
		}
		
		/**
		 * Executes this timed task
		 */
		public void run() {
			refresh(manager);
		}
	}
}
