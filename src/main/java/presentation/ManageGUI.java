package presentation;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import businesslogic.ProductManager;
import domain.Product;

public class ManageGUI {

	//The logger instance
	private Logger logger = Logger.getLogger(ManageGUI.class.getName());

	//The GUI components
	private JPanel bannerPanel;
	private JButton btnInsert;
	private JButton btnLog;
	private JButton btnOrders;
	private JButton btnPurchase;
	private JButton btnSearch;
	private JButton btnShow;
	private JButton btnSuppliers;
	private JButton btnUpdate;
	private JComboBox<String> cbOrder;
	private JPanel contentPane;
	private JFrame frame;
	private JLabel lblBanner;
	private JLabel lblLegend1;
	private JLabel lblLegend2;
	private JLabel lblLegend3;
	private JLabel lblLegend4;
	private JLabel lblLogo;
	private JLabel lblSort;
	private JScrollPane scrollPane;
	private JTable table;
	private JTextField tfSearch;
	
	//The loading screens
	private LoadScreen loadScreen;
	private LoadSmall loadSmall;
	
	//The product manager
	private ProductManager manager;
	
	//The search order that is used to display stock
	private String order = "ORDER BY stock - min ASC";

    /**
     * Initializes the fields of this class and creates the GUI
     * @param manager the product manager
     */
	public ManageGUI(final ProductManager manager) {
		this.manager = manager;

		//set the look and feel
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}

		//create the loading screen
		loadScreen = new LoadScreen();

		//create the frame and set the attributes
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Hartige Hap - Beheer");
		frame.setIconImage(new ImageIcon(getClass().getResource(
				"/Images/logo.png")).getImage());
		frame.setResizable(false);
		frame.setSize(662, 532);
		frame.setLocationRelativeTo(null);

		//create the content pane and set the attributes
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		frame.setContentPane(contentPane);

		setProgress(10);

		//create the search textfield and set the attributes
		tfSearch = new JTextField();
		tfSearch.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				//when the enter key is pressed a button click will be executed depending on whether the search field is empty
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					if (!tfSearch.getText().isEmpty()) {
						btnSearch.doClick();
					} else {
						btnShow.doClick();
					}
					tfSearch.selectAll();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {

			}

			@Override
			public void keyTyped(KeyEvent e) {

			}
		});
		tfSearch.setBounds(227, 131, 202, 23);
		tfSearch.setColumns(10);
		contentPane.add(tfSearch);

		setProgress(18);

		//create the search button and set the attributes
		btnSearch = new JButton("Zoeken");
		btnSearch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				refresh();
			}
		});
		btnSearch.setBounds(440, 130, 97, 25);
		contentPane.add(btnSearch);

		//create the insert button and set the attributes
		btnInsert = new JButton("Invoeren");
		btnInsert.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean login = new Login().doLogin(null, manager, false);
				if (login) {
					openInsertDialog();
				}
			}
		});
		btnInsert.setBounds(12, 93, 97, 25);
		contentPane.add(btnInsert);

		setProgress(26);

		//create the stock table and set the attributes
		table = new JTable() {

			private static final long serialVersionUID = 1L;

			@Override
			public Component prepareRenderer(TableCellRenderer renderer,
					int row, int column) {
				Component c = super.prepareRenderer(renderer, row, column);
				//if the stock is at min or below min value the row color will be red
				if ((Integer) table.getValueAt(
						row,
						table.convertColumnIndexToView(table.getColumn(
								"Voorraad").getModelIndex())) <= (Integer) table
						.getValueAt(
								row,
								table.convertColumnIndexToView(table.getColumn(
										"Min").getModelIndex()))) {
					c.setBackground(Color.RED);
					c.setForeground(Color.WHITE);
				//if the stock is at less than 6 above the minimum the row color will be orange
				} else if (((Integer) table.getValueAt(
						row,
						table.convertColumnIndexToView(table.getColumn(
								"Voorraad").getModelIndex())) - (Integer) table
						.getValueAt(
								row,
								table.convertColumnIndexToView(table.getColumn(
										"Min").getModelIndex()))) < 6) {
					c.setBackground(new Color(255, 140, 0));
					c.setForeground(Color.BLACK);
				//otherwise the row color will be white
				} else {
					c.setBackground(Color.WHITE);
					c.setForeground(Color.BLACK);
				}
				return c;
			}
		};

		//set the table model
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] {
				"ID", "Naam", "Voorraad", "Min" }) {

			private static final long serialVersionUID = 1L;

			boolean[] columnEditables = new boolean[] { false, false, false,
					false };

			@Override
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		
		//add a selection listener to the table
		table.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent event) {
						//if the value hasn't changed we do nothing
						if (!event.getValueIsAdjusting()) {
							return;
						}
						//when a product is clicked we open the product dialog for the selected product
						int value = (Integer) table.getValueAt(
								table.getSelectedRow(),
								table.convertColumnIndexToView(table.getColumn(
										"ID").getModelIndex()));
						openProductDialog(value);
					}
				});
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setHeaderValue("ID");
		table.getColumnModel().getColumn(2).setResizable(false);

		//create the scrollpane and set the attributes
		scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 168, 632, 294);
		scrollPane.setViewportView(table);
		contentPane.add(scrollPane);

		setProgress(38);

		//create the sort label and set the attributes
		lblSort = new JLabel("Sorteer op:");
		lblSort.setBounds(14, 134, 61, 16);
		contentPane.add(lblSort);

		setProgress(40);

		//create the sort order combobox and set the attributes
		cbOrder = new JComboBox<String>();
		cbOrder.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				//change the sort order depending on the selected combobox item
				switch (cbOrder.getSelectedIndex()) {
				case 0:
					order = "ORDER BY ingredient_id ASC";
					break;
				case 1:
					order = "ORDER BY ingredient_id DESC";
					break;
				case 2:
					order = "ORDER BY ingredient_name ASC";
					break;
				case 3:
					order = "ORDER BY ingredient_name DESC";
					break;
				case 4:
					order = "ORDER BY stock ASC";
					break;
				case 5:
					order = "ORDER BY stock DESC";
					break;
				case 6:
					order = "ORDER BY min ASC";
					break;
				case 7:
					order = "ORDER BY min DESC";
					break;
				case 8:
					order = "ORDER BY stock - min ASC";
					break;
				case 9:
					order = "ORDER BY stock - min DESC";
					break;
				default:
					order = "ORDER BY stock - min DESC";
					break;
				}
				refresh();
			}
		});

		setProgress(46);

		//set the combobox model
		cbOrder.setModel(new DefaultComboBoxModel<String>(new String[] {
				"ID - oplopend", "ID - aflopend", "Naam - a-z", "Naam - z-a",
				"Voorraad - oplopend", "Voorraad - aflopend", "Min - oplopend",
				"Min - aflopend", "(Vrd-Min) - oplopend",
				"(Vrd-Min) - aflopend" }));
		cbOrder.setSelectedIndex(8);
		cbOrder.setBounds(76, 131, 139, 23);
		contentPane.add(cbOrder);

		setProgress(52);

		//create the first legend label and set the attributes
		lblLegend1 = new JLabel("");
		lblLegend1.setBackground(Color.RED);
		lblLegend1.setOpaque(true);
		lblLegend1.setBounds(13, 476, 15, 15);
		contentPane.add(lblLegend1);

		//create the second legend label and set the attributes
		lblLegend2 = new JLabel("Voorraad <= min.");
		lblLegend2.setBounds(35, 476, 104, 16);
		contentPane.add(lblLegend2);

		//create the third legend label and set the attributes
		lblLegend3 = new JLabel("");
		lblLegend3.setOpaque(true);
		lblLegend3.setBackground(new Color(255, 140, 0));
		lblLegend3.setBounds(139, 476, 15, 15);
		contentPane.add(lblLegend3);

		//create the fourth legend label and set the attributes
		lblLegend4 = new JLabel("Voorraad < 6 boven min.");
		lblLegend4.setBounds(161, 476, 144, 16);
		contentPane.add(lblLegend4);

		//create the banner panel and set the attributes
		bannerPanel = new JPanel();
		bannerPanel.setLayout(null);
		bannerPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
		bannerPanel.setBackground(Color.WHITE);
		bannerPanel.setBounds(12, 12, 632, 68);

		//create the banner label and set the attributes
		lblBanner = new JLabel("Productbeheer");
		lblBanner.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblBanner.setBounds(97, 26, 530, 16);
		bannerPanel.add(lblBanner);

		setProgress(63);

		//create the logo label and set the attributes
		lblLogo = new JLabel("");
		lblLogo.setBounds(1, 1, 86, 66);

		//get the logo image and resize it to fit the logo label
		Image image = new ImageIcon(getClass().getResource("/Images/logo.png"))
				.getImage();
		Image newimg = image.getScaledInstance(lblLogo.getWidth(),
				lblLogo.getHeight(), java.awt.Image.SCALE_SMOOTH);
		ImageIcon imageIcon = new ImageIcon(newimg);

		//set the logo image as icon for the logo label
		lblLogo.setIcon(imageIcon);
		bannerPanel.add(lblLogo);
		contentPane.add(bannerPanel);

		//create the show all button and set the attributes
		btnShow = new JButton("Toon alles");
		btnShow.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//to show all stock we empty the search field and refresh
				tfSearch.setText("");
				refresh();
			}
		});
		btnShow.setBounds(547, 130, 97, 25);
		contentPane.add(btnShow);

		setProgress(73);

		//create the purchase button and set the attributes
		btnPurchase = new JButton("Bestellen");
		btnPurchase.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//open the purchase dialog
				openPurchaseDialog();
			}
		});
		btnPurchase.setBounds(226, 93, 97, 25);
		contentPane.add(btnPurchase);

		//create the log button and set the attributes
		btnLog = new JButton("Log");
		btnLog.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//create a new instance of the LogDialog
				new LogDialog(frame, manager);
			}
		});
		btnLog.setBounds(547, 93, 97, 25);
		contentPane.add(btnLog);

		//create the orders button and set the attributes
		btnOrders = new JButton("Orders");
		btnOrders.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//create an instance of the login class and verify the login
				boolean login = new Login().doLogin(null, manager, false);
				if (login) {
					//if the login is correct open the summary dialog
					openOrderSummaryDialog();
				}
			}
		});
		btnOrders.setBounds(119, 93, 97, 25);
		contentPane.add(btnOrders);

		setProgress(82);

		//create the suppliers button and set the attributes
		btnSuppliers = new JButton("Leveranciers");
		btnSuppliers.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//create an instance of the login class and verify the login
				boolean login = new Login().doLogin(null, manager, false);
				if (login) {
					//if the login is correct create a new instance of the supplier dialog
					new SupplierDialog(frame, manager);
				}
			}
		});
		btnSuppliers.setBounds(333, 93, 97, 25);
		contentPane.add(btnSuppliers);

		setProgress(95);

		//create the update button and set the attributes
		btnUpdate = new JButton("Afschrijven");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openUpdateDialog();
			}
		});
		btnUpdate.setBounds(440, 93, 97, 25);
		contentPane.add(btnUpdate);

		setProgress(100);

		frame.setVisible(true);

		if (table.getRowCount() < 1) {
			btnSearch.doClick();
		}
		tfSearch.requestFocus();

		//create a refresh timer
		Timer timer = new Timer();
		timer.schedule(new Refresh(), 5000, 5000);
	}

	/**
	 * Open the insert dialog by creating a new instance of InstanceDialog
	 */
	private void openInsertDialog() {
		new InsertDialog(this, frame, manager, null);
	}

	/**
	 * Open the product dialog by creating a new instance of ProductDialog
	 */
	private void openProductDialog(int value) {
		new ProductDialog(this, frame, manager, value);
	}

	/**
	 * Open the purchase dialog by creating a new instance of PurchaseDialog
	 */
	private void openPurchaseDialog() {
		new PurchaseDialog(this, frame, manager);
	}

	/**
	 * Open the order summary dialog by creating a new instance of OrderSummaryDialog
	 */
	private void openOrderSummaryDialog() {
		new OrderSummaryDialog(this, frame, manager);
	}

	/**
	 * Open the update dialog by creating a new instance of UpdateDialog
	 */
	private void openUpdateDialog() {
		new UpdateDialog(this, frame, manager);
	}

	/**
	 * Set the progress value of the progress bar on the loading screen
	 * @param value the progress bar value
	 */
	private void setProgress(int value) {
		if (loadScreen != null) {
			loadScreen.setProgress(value);
			if (value == 100) {
				loadScreen.dispose();
			}
		} else {
			if (value == 100) {
				loadSmall.dispose();
			}
		}
	}

	/**
	 * Fills the stock table with products and updates the table
	 * @param productList the list of products to show
	 */
	private void show(List<Product> productList) {
		int pos = scrollPane.getVerticalScrollBar().getValue();
		try {
			DefaultTableModel dtm = (DefaultTableModel) table.getModel();
			dtm.setRowCount(0);
			for (Product product : productList) {
				Object[] list = {product.getId(), product.getTitle(),
						product.getStock(), product.getMin()};
				dtm.addRow(list);
			}
			table.setModel(dtm);
		} finally {
			scrollPane.getVerticalScrollBar().setValue(pos);
		}
	}

	/**
	 * Refreshes the stock table by retrieving the product list from the manager, 
	 * this method takes into account the current search text and sort order
	 */
	public void refresh() {
		List<Product> productlist = (ArrayList<Product>) manager
				.giveProductList(tfSearch.getText(), order);
		show((ArrayList<Product>) productlist);
	}

	private class Refresh extends TimerTask {
		
		/**
		 * Executes this timed task
		 */
		public void run() {
			refresh();
		}
	}
}