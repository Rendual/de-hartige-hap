package presentation;

import java.awt.Component;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import businesslogic.ProductManager;

public class Login {

	/**
	 * Creates the login form and verifies whether an employee has successfully logged in
	 * @param c the GUI component to show the login form on
	 * @param manager the product manager
	 * @param isManager a flag indicating whether the employee must be a manager to login
	 * @return the login result as a boolean
	 */
	public boolean doLogin(Component c, ProductManager manager, boolean isManager) {
		boolean result = false;
		//create the GUI components
		JTextField barcode = new JPasswordField(8);
		barcode.addAncestorListener( new AncestorListener() {

			@Override
			public void ancestorAdded(AncestorEvent e) {
				JComponent component = e.getComponent();
				component.requestFocusInWindow();
			}

			@Override
			public void ancestorMoved(AncestorEvent e) {
				
			}

			@Override
			public void ancestorRemoved(AncestorEvent e) {
				
			}
		});
		//create the login form
		Object[] message = {"ID-code:", barcode};
		int option = JOptionPane.showConfirmDialog(c, message, "Login", JOptionPane.OK_CANCEL_OPTION);
		if (option == JOptionPane.OK_OPTION) {
			try {
				//verify the login
				result = manager.checkLogin(Integer.parseInt(barcode.getText()), isManager);
			} catch (NumberFormatException e) {
				result = false;
			}
			if (!result) {
				//if the login is invalid or an error occurred display a message
				JOptionPane.showMessageDialog(null,
						"Uw code werd niet gevonden of u heeft niet de juiste rechten, probeer het opniew!",
						"Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		return result;
	}
}