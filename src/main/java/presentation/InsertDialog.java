package presentation;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.Spring;
import javax.swing.SpringLayout;
import javax.swing.SpringLayout.Constraints;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import businesslogic.ProductManager;
import domain.ProdType;
import domain.ProdUnit;
import domain.Product;
import domain.Supplier;

public class InsertDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	//The GUI components
	private JButton btnNewSupplier;
	private JButton btnNewType;
	private JButton btnSave;
	private JComboBox<String> cbSupplier;
	private JComboBox<String> cbType;
	private JComboBox<String> cbUnit;
	private JPanel contentPane;
	private ImageIcon image;
	private File imgfile;
	private JLabel lblImage;
	private JLabel lblMin;
	private JLabel lblPrice;
	private JLabel lblStock;
	private JLabel lblSupplier;
	private JLabel lblType;
	private JSeparator separator1;
	private JSeparator separator2;
	private JSpinner spMin;
	private JSpinner spStock;
	private List<Supplier> suppliers;
	private JTextField tfPrice;
	private JTextField txtName;
	private JTextPane txtpnDescription;
	private List<ProdType> types;
	private List<ProdUnit> units;
	private JButton btnNewUnit;

    /**
     * Initializes the fields of this class and creates the GUI
     * @param gui the manage GUI
     * @param parent the JFrame this dialog is displayed on
     * @param manager the product manager
     * @param product the product that is being edited
     */
	public InsertDialog(final ManageGUI gui, final JFrame parent, final ProductManager manager,
			final Product product) {
		super(parent, "Een product invoeren", false);

		//create the loading screen
		LoadSmall load = new LoadSmall(parent, "Aan het laden...");

		//set the frame attrivutes
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setResizable(false);
		setSize(505, 391);
		setLocationRelativeTo(parent);

		//create the content pane and set the attributes
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);

		//create the name textfield and set the attributes
		txtName = new JTextField();
		txtName.setText("Naam");
		txtName.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				if (txtName.getText().equals("Naam")) {
					txtName.setText("");
				}
			}

			@Override
			public void focusLost(FocusEvent e) {
				if (txtName.getText().isEmpty()) {
					txtName.setText("Naam");
				}
			}
		});
		txtName.setBounds(12, 12, 240, 22);
		txtName.setColumns(10);
		contentPane.add(txtName);

		//create the description textpane and set the attributes
		txtpnDescription = new JTextPane();
		txtpnDescription.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				if (txtpnDescription.getText().equals("Beschrijving")) {
					txtpnDescription.setText("");
				}
			}

			@Override
			public void focusLost(FocusEvent e) {
				if (txtpnDescription.getText().isEmpty()) {
					txtpnDescription.setText("Beschrijving");
				}
			}
		});
		txtpnDescription.setText("Beschrijving");
		txtpnDescription.setBorder(new MatteBorder(1, 1, 1, 1,
				(Color) new Color(0, 0, 0)));
		txtpnDescription.setBounds(12, 226, 240, 124);
		contentPane.add(txtpnDescription);

		//set the spinner models
		SpinnerNumberModel model1 = new SpinnerNumberModel(0, 0,
				Integer.MAX_VALUE, 1);
		SpinnerNumberModel model2 = new SpinnerNumberModel(0, 0,
				Integer.MAX_VALUE, 1);

		//create the stock spinner and set the attributes
		spStock = new JSpinner(model1);
		spStock.setBounds(350, 12, 39, 22);
		contentPane.add(spStock);

		//create the stock label and set the attributes
		lblStock = new JLabel("Voorraad:");
		lblStock.setBounds(282, 15, 58, 16);
		contentPane.add(lblStock);

		//create the unit combobox and set the attributes
		cbUnit = new JComboBox<String>();
		cbUnit.setBounds(350, 84, 137, 22);
		contentPane.add(cbUnit);

		//create the minimum stock spinner and set the attributes
		spMin = new JSpinner(model2);
		spMin.setBounds(350, 48, 39, 22);
		contentPane.add(spMin);

		//create the save button and set the attributes
		btnSave = new JButton("Opslaan");
		btnSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LoadSmall load = new LoadSmall(parent, "Product opslaan...");
				ProdType selectedType = null;
				ProdUnit selectedUnit = null;
				Supplier selectedSupplier = null;
				//get the selected options from the comboboxes
				for (ProdType type : types) {
					if (type.getName().equals(cbUnit.getSelectedItem())) {
						selectedType = type;
					}
				}
				for (ProdUnit unit : units) {
					if (unit.getName().equals(cbType.getSelectedItem())) {
						selectedUnit = unit;
					}
				}
				for (Supplier supplier : suppliers) {
					if (supplier.getName().equals(cbSupplier.getSelectedItem())) {
						selectedSupplier = supplier;
					}
				}
				
				//get the price
				BigDecimal price;
				try {
					price = new BigDecimal(tfPrice.getText());
				} catch (NumberFormatException ex) {
					JOptionPane.showMessageDialog(null,
							"U moet een getal als prijs invullen!",
							"Error", JOptionPane.ERROR_MESSAGE);
					load.dispose();
					return;
				}
				
				//determine whether we're inserting or updating a product
				if (product != null) {
					//update the product
					Product updateprod = new Product(product.getId(), txtName
							.getText(), txtpnDescription.getText(), null,
							(Integer) spStock.getValue(), selectedUnit,
							(Integer) spMin.getValue(), selectedType,
							selectedSupplier);
					manager.updatePrice(price
							.setScale(2, RoundingMode.HALF_UP), product.getId());
					manager.updateProduct(updateprod);
					if (imgfile != null) {
						manager.updateImage(imgfile, product.getId());
					}
					load.dispose();
					//return to the product info dialog
					new ProductDialog(gui, parent, manager, product.getId());
					dispose();
				} else {
					try {
						//add the product
						manager.insertProduct(txtName.getText(),
								txtpnDescription.getText(),
								(Integer) spStock.getValue(), selectedUnit,
								selectedType, (Integer) spMin.getValue(),
								selectedSupplier,
								price);
					} catch (SQLException ex) {
						//Does nothing because duplicate entry failures.
					}
					if(imgfile != null) {
						manager.setImage(imgfile);
					}
					load.dispose();
					dispose();
				}
				//refresh the manage GUI
				gui.refresh();
			}
		});
		btnSave.setBounds(350, 193, 137, 25);
		contentPane.add(btnSave);

		//create the image label and set the attributes
		lblImage = new JLabel(
				"<html>\r\n<u>Geen afbeelding gevonden!</u>\r\n</html>");
		lblImage.setBackground(Color.WHITE);
		lblImage.setOpaque(true);
		lblImage.setHorizontalAlignment(JLabel.CENTER);
		lblImage.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				boolean shouldStop = true;
				do {
					shouldStop = true;
					//create a file chooser to pick an image for the product
					JFileChooser fileChooser = new JFileChooser();
					fileChooser.setCurrentDirectory(new File(System
							.getProperty("user.home")));
					fileChooser.removeChoosableFileFilter(fileChooser
							.getFileFilter());
					fileChooser
							.addChoosableFileFilter(new FileNameExtensionFilter(
									"Images", "jpg", "png", "gif", "bmp",
									"jpeg"));
					int result = fileChooser.showOpenDialog(fileChooser);
					if (result == JFileChooser.APPROVE_OPTION) {
						File selectedFile = fileChooser.getSelectedFile();
						if (selectedFile.length() < 65535) {
							//set the new image
							image = new ImageIcon(selectedFile.getPath());
							imgfile = selectedFile;
							Image img = image.getImage();
							Image newimg = img.getScaledInstance(lblImage.getWidth(),
									lblImage.getHeight(), java.awt.Image.SCALE_SMOOTH);
							ImageIcon imageIconRe = new ImageIcon(newimg);
							lblImage.setIcon(imageIconRe);
						} else {
							shouldStop = false;
							JOptionPane
									.showMessageDialog(InsertDialog.this,
											"De afbeelding die u heeft gekozen is te groot!\nSelecteer een ander.");

						}
					}
				} while (shouldStop == false);
			}
		});
		lblImage.setForeground(Color.BLUE);
		lblImage.setBounds(12, 48, 240, 164);
		contentPane.add(lblImage);

		//create the type label and set the attributes
		lblType = new JLabel("Type:");
		lblType.setBounds(282, 87, 33, 16);
		contentPane.add(lblType);

		//create the minimum stock and set the attributes
		lblMin = new JLabel("Min:");
		lblMin.setBounds(282, 51, 25, 16);
		contentPane.add(lblMin);

		//create the type combobox and set the attributes
		cbType = new JComboBox<String>();
		cbType.setBounds(403, 11, 84, 25);
		contentPane.add(cbType);

		//create the supplier label and set the attributes
		lblSupplier = new JLabel("Leverancier:");
		lblSupplier.setBounds(282, 160, 66, 14);
		contentPane.add(lblSupplier);

		//create the supplier combobox and set the attributes
		cbSupplier = new JComboBox<String>();
		cbSupplier.setBounds(350, 157, 137, 22);
		contentPane.add(cbSupplier);

		//create the new supplier button and set the attributes
		btnNewSupplier = new JButton("Nieuwe leverancier");
		btnNewSupplier.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//create the new supplier form
				JLabel lblName = new JLabel("Naam:");
				JTextField tfName = new JTextField(15);
				JLabel lblAdres = new JLabel("Adres:");
				JTextField tfAdres = new JTextField(15);
				JLabel lblZipcode = new JLabel("Postcode:");
				JTextField tfZipcode = new JTextField(15);
				JLabel lblPhone = new JLabel("Telefoon:");
				JTextField tfPhone = new JTextField(15);
				JLabel lblEmail = new JLabel("Email:");
				JTextField tfEmail = new JTextField(15);
				JLabel lblShippingCost = new JLabel("Verzendkosten:");
				JTextField tfShippingCost = new JTextField(15);

				JPanel supplierPanel = new JPanel(new SpringLayout());

				supplierPanel.add(lblName);
				supplierPanel.add(tfName);
				supplierPanel.add(lblAdres);
				supplierPanel.add(tfAdres);
				supplierPanel.add(lblZipcode);
				supplierPanel.add(tfZipcode);
				supplierPanel.add(lblPhone);
				supplierPanel.add(tfPhone);
				supplierPanel.add(lblEmail);
				supplierPanel.add(tfEmail);
				supplierPanel.add(lblShippingCost);
				supplierPanel.add(tfShippingCost);

				makeCompactGrid(supplierPanel, 6, 2, 6, 6, 6, 6);

				int result = JOptionPane.showConfirmDialog(null, supplierPanel,
						"", JOptionPane.OK_CANCEL_OPTION);

				if (result == JOptionPane.OK_OPTION) {
					//add the new supplier
					LoadSmall load = new LoadSmall(parent, "Aan het updaten...");
					manager.addSupplier(new Supplier(0, tfName.getText(),
							tfAdres.getText(), tfZipcode.getText(), tfPhone
									.getText(), tfEmail.getText(),
							new BigDecimal(tfShippingCost.getText())));
					updateSupplierBox(manager);
					cbSupplier.setSelectedItem(tfName.getText());
					load.dispose();
				}
			}
		});
		btnNewSupplier.setBounds(282, 248, 205, 25);
		contentPane.add(btnNewSupplier);

		//create the new unit button and set the attributes
		btnNewUnit = new JButton("Nieuwe eenheid");
		btnNewUnit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//create the new unit form
				JLabel lblUnit = new JLabel("Eenheid:");
				JTextField tfUnit = new JTextField(15);

				JPanel unitPanel = new JPanel();
				unitPanel.setLayout(new FlowLayout(0));
				unitPanel.add(lblUnit);
				unitPanel.add(tfUnit);

				int result = JOptionPane.showConfirmDialog(null, unitPanel, "",
						JOptionPane.OK_CANCEL_OPTION);

				if (result == JOptionPane.OK_OPTION) {
					//add the new unit
					LoadSmall load = new LoadSmall(parent, "Aan het updaten...");
					manager.addUnit(new ProdUnit(0, tfUnit.getText()));
					load.dispose();
				}
				updateTypeBox(manager);
				cbType.setSelectedItem(tfUnit.getText());
			}
		});
		btnNewUnit.setBounds(282, 326, 205, 25);
		contentPane.add(btnNewUnit);

		//create the new type button and set the attributes
		btnNewType = new JButton("Nieuw type");
		btnNewType.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//create the new type form
				JLabel lblType = new JLabel("Type:");
				JTextField tfType = new JTextField(15);

				JPanel typePanel = new JPanel();
				typePanel.setLayout(new FlowLayout(0));
				typePanel.add(lblType);
				typePanel.add(tfType);

				int result = JOptionPane.showConfirmDialog(null, typePanel, "",
						JOptionPane.OK_CANCEL_OPTION);

				if (result == JOptionPane.OK_OPTION) {
					//add the new type
					LoadSmall load = new LoadSmall(parent, "Aan het updaten...");
					manager.addType(new ProdType(0, tfType.getText()));
					load.dispose();
				}
				updateUnitBox(manager);
				cbUnit.setSelectedItem(tfType.getText());
			}
		});
		btnNewType.setBounds(282, 287, 205, 25);
		contentPane.add(btnNewType);

		//create the price label and set the attributes
		lblPrice = new JLabel("Prijs:");
		lblPrice.setBounds(282, 123, 46, 14);
		contentPane.add(lblPrice);

		//create the price textfield and set the attributes
		tfPrice = new JTextField();
		tfPrice.setBounds(350, 120, 137, 23);
		contentPane.add(tfPrice);
		tfPrice.setColumns(10);

		//create the second separator and set the attributes
		separator2 = new JSeparator();
		separator2.setOrientation(SwingConstants.VERTICAL);
		separator2.setBounds(266, 12, 2, 338);
		contentPane.add(separator2);

		//create the first separator and set the attributes
		separator1 = new JSeparator();
		separator1.setBounds(282, 232, 205, 2);
		contentPane.add(separator1);

		//get the types, units and suppliers
		types = (ArrayList<ProdType>) manager.getTypes();
		units = (ArrayList<ProdUnit>) manager.getUnits();
		suppliers = (ArrayList<Supplier>) manager.getSuppliers();

		if (product != null) {
			//if we're updating a product fill the fields
			txtName.setText(product.getTitle());
			spStock.setValue(product.getStock());
			txtpnDescription.setText(product.getDescription());
			spMin.setValue(product.getMin());
			for (ProdType type : types) {
				cbUnit.addItem(type.getName());
				if (type.getValue() == product.getType().getValue()) {
					cbUnit.setSelectedItem(type.getName());
				}
			}
			for (ProdUnit unit : units) {
				cbType.addItem(unit.getName());
				if (unit.getId() == product.getUnit().getId()) {
					cbType.setSelectedItem(unit.getName());
				}
			}
			for (Supplier supplier : suppliers) {
				cbSupplier.addItem(supplier.getName());
				if (supplier.getId() == product.getSupplier().getId()) {
					cbSupplier.setSelectedItem(supplier.getName());
				}
			}
			tfPrice.setText(""
					+ manager.getPriceShippCost(product.getId(), product
							.getSupplier().getId())[0]);
			if (product.getImage() != null) {
				ImageIcon imageIcon = product.getImage();
				Image imagenew = imageIcon.getImage();
				Image newimg = imagenew.getScaledInstance(lblImage.getWidth(),
						lblImage.getHeight(), java.awt.Image.SCALE_SMOOTH);
				imageIcon = new ImageIcon(newimg);
				lblImage.setIcon(imageIcon);
				image = product.getImage();
			}
		} else {
			//if we're inserting a product only update the combo boxes
			updateComboBoxes(manager);
		}

		load.dispose();

		setVisible(true);
	}

	/**
	 * Creates constraints for a cel 
	 * @param row the row
	 * @param col the column
	 * @param parent the parent Container to display the constraints on
	 * @param cols the amount of columns
	 * @return the constracts as a Contraints object
	 */
	private Constraints getConstraintsForCell(int row, int col,
			Container parent, int cols) {
		SpringLayout layout = (SpringLayout) parent.getLayout();
		Component c = parent.getComponent(row * cols + col);
		return layout.getConstraints(c);
	}

	/**
	 * Creates a compact grid
	 * @param parent the parent Container to display this grid on
	 * @param rows the amount of rows
	 * @param cols the amount of columns
	 * @param initialX the initial x value
	 * @param initialY the initial y value
	 * @param xPad the x padding
	 * @param yPad the y padding
	 */
	private void makeCompactGrid(Container parent, int rows, int cols,
			int initialX, int initialY, int xPad, int yPad) {
		SpringLayout layout;
		try {
			layout = (SpringLayout) parent.getLayout();
		} catch (ClassCastException exc) {
			return;
		}

		Spring x = Spring.constant(initialX);
		for (int c = 0; c < cols; c++) {
			Spring width = Spring.constant(0);
			for (int r = 0; r < rows; r++) {
				width = Spring.max(width,
						getConstraintsForCell(r, c, parent, cols).getWidth());
			}
			for (int r = 0; r < rows; r++) {
				SpringLayout.Constraints constraints = getConstraintsForCell(r,
						c, parent, cols);
				constraints.setX(x);
				constraints.setWidth(width);
			}
			x = Spring.sum(x, Spring.sum(width, Spring.constant(xPad)));
		}

		Spring y = Spring.constant(initialY);
		for (int r = 0; r < rows; r++) {
			Spring height = Spring.constant(0);
			for (int c = 0; c < cols; c++) {
				height = Spring.max(height,
						getConstraintsForCell(r, c, parent, cols).getHeight());
			}
			for (int c = 0; c < cols; c++) {
				SpringLayout.Constraints constraints = getConstraintsForCell(r,
						c, parent, cols);
				constraints.setY(y);
				constraints.setHeight(height);
			}
			y = Spring.sum(y, Spring.sum(height, Spring.constant(yPad)));
		}

		SpringLayout.Constraints pCons = layout.getConstraints(parent);
		pCons.setConstraint(SpringLayout.SOUTH, y);
		pCons.setConstraint(SpringLayout.EAST, x);
	}

	/**
	 * Updates the combo boxes
	 * @param manager the product manager
	 */
	private void updateComboBoxes(ProductManager manager) {
		types = (ArrayList<ProdType>) manager.getTypes();
		units = (ArrayList<ProdUnit>) manager.getUnits();
		suppliers = (ArrayList<Supplier>) manager.getSuppliers();

		cbUnit.removeAllItems();
		cbType.removeAllItems();
		cbSupplier.removeAllItems();

		for (ProdType type : types) {
			cbUnit.addItem(type.getName());
		}
		for (ProdUnit unit : units) {
			cbType.addItem(unit.getName());
		}
		for (Supplier supplier : suppliers) {
			cbSupplier.addItem(supplier.getName());
		}
	}

	/**
	 * Updates the supplier combobox
	 * @param manager the product manager
	 */
	private void updateSupplierBox(ProductManager manager) {
		suppliers = (ArrayList<Supplier>) manager.getSuppliers();
		cbSupplier.removeAllItems();
		for (Supplier supplier : suppliers) {
			cbSupplier.addItem(supplier.getName());
		}
	}

	/**
	 * Updates the type combobox
	 * @param manager the product manager
	 */
	private void updateTypeBox(ProductManager manager) {
		units = (ArrayList<ProdUnit>) manager.getUnits();
		cbType.removeAllItems();
		for (ProdUnit unit : units) {
			cbType.addItem(unit.getName());
		}
	}

	/**
	 * Updates the unit combobox
	 * @param manager the product manager
	 */
	private void updateUnitBox(ProductManager manager) {
		types = (ArrayList<ProdType>) manager.getTypes();
		cbUnit.removeAllItems();
		for (ProdType type : types) {
			cbUnit.addItem(type.getName());
		}
	}
}