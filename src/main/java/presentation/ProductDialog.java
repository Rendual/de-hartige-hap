package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.RoundingMode;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import businesslogic.ProductManager;
import domain.Product;

public class ProductDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	//The GUI components
	private JButton btnDelete;
	private JButton btnEdit;
	private JPanel contentPane;
	private JTextArea lblDescription;
	private JLabel lblImage;
	private JLabel lblMin;
	private JLabel lblPrice;
	private JLabel lblProductname;
	private JLabel lblStock;
	private JLabel lblSupplier;
	private JLabel lblType;
	private Product product;
	private JSeparator separator1;
	private JSeparator separator2;

    /**
     * Initializes the fields of this class and creates the GUI
     * @param gui the manage GUI
     * @param parent the JFrame this dialog is displayed on
     * @param manager the product manager
     * @param value the product id
     */
	public ProductDialog(final ManageGUI gui, final JFrame parent, final ProductManager manager,
			final int value) {
		super(parent, "Productinformatie bekijken", false);

		product = manager.giveProduct(value);

		//create the loading screen
		LoadSmall load = new LoadSmall(parent, "Aan het laden...");

		//set the frame attributes
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setSize(505, 391);
		setLocationRelativeTo(parent);

		//create the content pane and set the attributes
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);

		//create the first separator and set the attributes
		separator1 = new JSeparator();
		separator1.setOrientation(SwingConstants.VERTICAL);
		separator1.setBounds(266, 12, 2, 338);
		contentPane.add(separator1);

		//create the stock label and set the attributes
		lblStock = new JLabel("Voorraad:");
		lblStock.setBounds(282, 15, 115, 16);
		lblStock.setMaximumSize(new Dimension(100, 200));
		contentPane.add(lblStock);

		//create the delete button and set the attributes
		btnDelete = new JButton("Verwijderen");
		btnDelete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Object[] options = { "Ja", "Nee" };
				int n = JOptionPane
						.showOptionDialog(ProductDialog.this,
								"Weet u zeker dat u dit product "
										+ "wilt verwijderen?",
								"Productbeheer - Verwijderen",
								JOptionPane.YES_NO_CANCEL_OPTION,
								JOptionPane.QUESTION_MESSAGE, null, options,
								options[1]);
				if (n == 0) {
					boolean login = new Login().doLogin(ProductDialog.this,
							manager, true);
					//delete the product
					if (login) {
						manager.deleteProduct(product.getId(),
								product.getTitle());
						gui.refresh();
						dispose();
					}
				}
			}

		});
		btnDelete.setBounds(282, 287, 205, 25);
		contentPane.add(btnDelete);

		//create the edit button and set the attributes
		btnEdit = new JButton("Wijzigen");
		btnEdit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean login = new Login()
						.doLogin(ProductDialog.this, manager, false);
				if (login) {
					new InsertDialog(gui, parent, manager, product);
					dispose();
				}
			}
		});
		btnEdit.setBounds(282, 248, 205, 25);
		contentPane.add(btnEdit);

		//create the product name label and set the attributes
		lblProductname = new JLabel("Productnaam");
		lblProductname.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblProductname.setBounds(12, 12, 240, 22);
		contentPane.add(lblProductname);

		//create the image label and set the attributes
		lblImage = new JLabel("");
		lblImage.setBounds(12, 48, 240, 164);
		contentPane.add(lblImage);

		//create the type label and set the attributes
		lblType = new JLabel("Type:");
		lblType.setBounds(282, 87, 119, 16);
		contentPane.add(lblType);

		//create the minimum stock label and set the attributes
		lblMin = new JLabel("Min: 0");
		lblMin.setBounds(282, 51, 105, 16);
		contentPane.add(lblMin);

		//create the supplier label and set the attributes
		lblSupplier = new JLabel("(Standaard) Leverancier:");
		lblSupplier.setBounds(282, 160, 121, 14);
		contentPane.add(lblSupplier);

		//create the price label and set the attributes
		lblPrice = new JLabel("Prijs: ");
		lblPrice.setBounds(282, 123, 46, 14);
		contentPane.add(lblPrice);

		//create the description textarea and set the attributes
		lblDescription = new JTextArea();
		lblDescription.setLineWrap(true);
		lblDescription.setEditable(false);
		lblDescription.setBounds(12, 226, 240, 124);
		contentPane.add(lblDescription);

		//create the second separator and set the attributes
		separator2 = new JSeparator();
		separator2.setBounds(282, 232, 205, 2);
		contentPane.add(separator2);

		showProduct(manager);

		load.dispose();
		setVisible(true);

	}

	/**
	 * Displays the product information
	 * @param manager the product manager
	 */
	private void showProduct(ProductManager manager) {
		lblStock.setText("Voorraad: " + product.getStock() + " "
				+ product.getUnit().getName());
		lblStock.setSize(lblStock.getPreferredSize());
		lblDescription.setText(product.getDescription());
		lblProductname.setText(product.getTitle());
		lblProductname.setPreferredSize(lblProductname.getPreferredSize());
		lblType.setText("Type: " + product.getType().getName());
		lblType.setSize(lblType.getPreferredSize());
		lblMin.setText("Min: " + product.getMin() + " "
				+ product.getUnit().getName());
		lblSupplier.setText("Leverancier: " + product.getSupplier().getName());
		lblSupplier.setSize(lblSupplier.getPreferredSize());
		lblPrice.setText("Prijs (p.p.): \u20ac "
				+ manager.getPriceShippCost(product.getId(), product
						.getSupplier().getId())[0].setScale(2,
						RoundingMode.HALF_UP));
		lblPrice.setSize(lblPrice.getPreferredSize());
		if (product.getStock() <= product.getMin()) {
			lblStock.setBackground(Color.RED);
			lblStock.setForeground(Color.WHITE);
			lblStock.setOpaque(true);
		} else if ((product.getStock() - product.getMin()) < 6) {
			lblStock.setBackground(new Color(255, 140, 0));
			lblStock.setForeground(Color.BLACK);
			lblStock.setOpaque(true);
		} else {
			lblStock.setOpaque(false);
		}
		if (product.getImage() != null) {
			ImageIcon imageIcon = product.getImage();
			Image image = imageIcon.getImage();
			Image newimg = image.getScaledInstance(lblImage.getWidth(),
					lblImage.getHeight(), java.awt.Image.SCALE_SMOOTH);
			imageIcon = new ImageIcon(newimg);
			lblImage.setIcon(imageIcon);
		} else {
			lblImage.setText("Geen afbeelding gevonden!");
			lblImage.setBackground(Color.WHITE);
			lblImage.setOpaque(true);
			lblImage.setHorizontalAlignment(JLabel.CENTER);
		}
	}
}