package presentation;

import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class LoadScreen extends JFrame {

	private static final long serialVersionUID = 1L;

	//The GUI components
	private JPanel contentPane;
	private JLabel lblLogo;
	private JProgressBar progressBar;

    /**
     * Initializes the fields of this class and creates the GUI
     */
	public LoadScreen() {
		//set the frame attributes
		setIconImage(new ImageIcon(getClass().getResource("/Images/logo.png"))
				.getImage());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(544, 342);
		setLocationRelativeTo(null);
		setUndecorated(true);

		//create the content pane and set the attributes
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);

		//create the logo label and set the attributes
		lblLogo = new JLabel("");
		lblLogo.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogo.setBounds(167, 62, 220, 172);
		
		//get the logo image and resize it to fit the logo label
		Image image = new ImageIcon(getClass().getResource("/Images/logo.png"))
				.getImage();
		Image newimg = image.getScaledInstance(lblLogo.getWidth(),
				lblLogo.getHeight(), Image.SCALE_SMOOTH);
		ImageIcon imageIcon = new ImageIcon(newimg);
		
		//set the logo image as icon for the logo label
		lblLogo.setIcon(imageIcon);
		contentPane.add(lblLogo);

		//create the progress bar and set the attributes
		progressBar = new JProgressBar();
		progressBar.setStringPainted(true);
		progressBar.setBounds(167, 248, 220, 32);
		contentPane.add(progressBar);

		setVisible(true);
	}

    /**
     * Accessor method to get the progress bar value
     * @return the progress bar value
     */
	public int getProgressValue() {
		return progressBar.getValue();
	}

    /**
     * Set the progress bar value
     * @param value the new progress bar value
     */
	public void setProgress(int value) {
		progressBar.setValue(value);
	}
}