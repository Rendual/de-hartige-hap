package presentation;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import businesslogic.ProductManager;
import domain.LogItem;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LogDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	
	//The GUI component
	private JComboBox<String> cbOrder;
	private JPanel contentPane;
	private JLabel label;
	private JScrollPane scrollPane;
	private JTable table1;
	private JButton btnTruncate;
	
	//The search order that is used to display log items
	private String order = "date ASC";

    /**
     * Initializes the fields of this class and creates the GUI
     * @param parent the JFrame this dialog is displayed on
     * @param manager the product manager
     */
	public LogDialog(JFrame parent, final ProductManager manager) {
		super(parent, "Log", false);
		
		//create the loading screen
		LoadSmall load = new LoadSmall(parent, "Aan het laden...");
		
		//set the frame attributes
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setResizable(false);
		setSize(480, 384);
		setLocationRelativeTo(parent);
		
		//create the content pane and set the attributes
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);

		//create the scroll pane and set the attributes
		scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 49, 450, 294);
		contentPane.add(scrollPane);

		//create the log table and set the attributes
		table1 = new JTable();
		
		//set the table model
		table1.setModel(new DefaultTableModel(new Object[][] {}, new String[] {
				"Naam", "Actie", "Datum"}) {
			
			private static final long serialVersionUID = 1L;
			
			boolean[] columnEditables = new boolean[] {
				false, false, false
			};
			
			@Override
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		scrollPane.setViewportView(table1);

		//create the order combobox and set the attributes
		cbOrder = new JComboBox<String>();
		cbOrder.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() != 2) {
					return;
				}
				switch (cbOrder.getSelectedIndex()) {
				case 0:
					order = "name ASC";
					break;
				case 1:
					order = "name DESC";
					break;
				case 2:
					order = "action ASC";
					break;
				case 3:
					order = "action DESC";
					break;
				case 4:
					order = "date ASC";
					break;
				case 5:
					order = "date DESC";
					break;
				default:
					order = "date ASC";
					break;
				}
				setTableData(manager);
			}
		});
		
		//set the order combobox model
		cbOrder.setModel(new DefaultComboBoxModel<String>(new String[] {
				"Naam - a-z", "Naam - z-a", "Actie - a-z", "Actie - z-a",
				"Datum - oplopend", "Datum - aflopend" }));
		cbOrder.setSelectedIndex(5);
		cbOrder.setBounds(76, 12, 139, 23);
		contentPane.add(cbOrder);

		//create the sort label and set the attributes
		label = new JLabel("Sorteer op:");
		label.setBounds(12, 15, 66, 16);
		contentPane.add(label);
		
		//create the truncate button and set the attributes
		btnTruncate = new JButton("Maak leeg");
		btnTruncate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean login = new Login().doLogin(LogDialog.this, manager, true);
				if (login) {
					manager.truncateLog();
					setTableData(manager);
				}
			}
		});
		btnTruncate.setBounds(373, 12, 89, 23);
		contentPane.add(btnTruncate);
		
		load.dispose();
		setVisible(true);
	}
	
	/**
	 * Sets the data for the table
	 * @param manager the product manager
	 */
	private void setTableData(ProductManager manager) {
		DefaultTableModel dtm = (DefaultTableModel) table1.getModel();
		dtm.setRowCount(0);
		List<LogItem> log = manager.getLog(order);
		DefaultTableModel model = (DefaultTableModel) table1.getModel();
		for (LogItem logitem : log) {
			Object[] newlogitem = { logitem.getUser(),
					logitem.getDescription(), logitem.getTime() };
			model.addRow(newlogitem);
		}
		table1.setModel(model);
	}
}