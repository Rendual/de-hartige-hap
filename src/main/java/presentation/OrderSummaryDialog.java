package presentation;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import businesslogic.ProductManager;
import domain.Order;
import domain.OrderItem;

public class OrderSummaryDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	//The GUI components
	private JPanel contentPane;
	private JTable table;
	private JTable table1;
	private JLabel lblStatus;
	private JLabel lblSubTotal;
	private JLabel lblShippingCost;
	private JLabel lblSupplier;
	private List<Order> orders;
	private JButton btnDelivered;
	private JPanel orderPanel;
	private JScrollPane scrollPane1;
	private JScrollPane scrollPane2;
	private JScrollPane scrollPane3;
	private JLabel lblLogo;
	private JLabel lblHeaderLabel;
	private JSeparator separator;
	private JLabel lblInfoLabel;
	private Component lblProducten;
	private JLabel lblSubPrice;
	private JLabel lblShippingCost1;
	private JLabel lblShippingCost2;
	private JLabel lblTotal;
	private JLabel lblPrice;
	private JButton btnDelete;

	/**
	 * Initializes the fields of this class and creates the GUI
	 * @param gui the manage GUI
	 * @param parent the JFrame this dialog is displayed on
	 * @param manager the product manager
	 */
	public OrderSummaryDialog(final ManageGUI gui, final JFrame parent,
			final ProductManager manager) {
		super(parent, "Orders", false);

		//create the loading screen
		LoadSmall load = new LoadSmall(parent, "Aan het laden...");

		//set the frame attributes
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setSize(737, 502);
		setLocationRelativeTo(parent);

		//create the content pane and set the attributes
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);

		orders = (ArrayList<Order>) manager.getOrders();

		//create the first scrollpane and set the attributes
		scrollPane1 = new JScrollPane();
		scrollPane1.setBounds(12, 12, 250, 413);
		contentPane.add(scrollPane1);

		//create the order table and set the attributes
		table = new JTable() {
			private static final long serialVersionUID = 1L;

			@Override
			public Component prepareRenderer(TableCellRenderer renderer,
					int row, int column) {
				Component c = super.prepareRenderer(renderer, row, column);
				String status = (String) table.getValueAt(row, table
						.convertColumnIndexToView(table.getColumn("Status")
								.getModelIndex()));
				if ("Besteld".equals(status)) {
					c.setBackground(new Color(255, 140, 0));
					c.setForeground(Color.BLACK);
				} else if ("Geleverd".equals(status)) {
					c.setBackground(Color.GREEN);
					c.setForeground(Color.BLACK);
				}
				return c;
			}
		};

		//set the order table model
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] {
				"ID", "Datum", "Leverancier", "Status" }) {
			private static final long serialVersionUID = 1L;
			boolean[] columnEditables = new boolean[] { false, false, false,
					false };

			@Override
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		table.getColumnModel().getColumn(0).setPreferredWidth(40);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent evnt) {
				DefaultTableModel model = (DefaultTableModel) table1.getModel();
				model.setRowCount(0);
				Order selectedOrder = null;
				for (Order order : orders) {
					if (order.getId() == (Integer) table.getValueAt(table
							.getSelectedRow(), (Integer) table
							.convertColumnIndexToView(table.getColumn("ID")
									.getModelIndex()))) {
						selectedOrder = order;
						DefaultTableModel dtm = (DefaultTableModel) table1
								.getModel();
						for (OrderItem orderItem : order.getOrderItems()) {
							Object[] list = new Object[] {
									orderItem.getId(),
									orderItem.getName(),
									orderItem.getNum(),
									orderItem.getPrice(),
									orderItem
											.getPrice()
											.multiply(
													new BigDecimal(""
															+ orderItem
																	.getNum()))
											.setScale(2, RoundingMode.HALF_UP) };
							dtm.addRow(list);
						}
						table1.setModel(dtm);
					}
				}
				lblSupplier.setText("<html>\r\n"
						+ selectedOrder.getSupplier().getName() + "<br>\r\n"
						+ selectedOrder.getSupplier().getAddress() + "<br>\r\n"
						+ selectedOrder.getSupplier().getZipcode() + "<br>\r\n"
						+ selectedOrder.getSupplier().getPhone() + "<br>\r\n"
						+ selectedOrder.getSupplier().getEmail() + "</html>");
				lblSupplier.setSize(lblSupplier.getPreferredSize());
				lblStatus.setText("Status: " + selectedOrder.getState());
				lblStatus.setOpaque(true);
				if ("Besteld".equals(selectedOrder.getState())) {
					lblStatus.setBackground(new Color(255, 140, 0));
					btnDelivered.setEnabled(true);
				} else if ("Geleverd".equals(selectedOrder.getState())) {
					lblStatus.setBackground(Color.GREEN);
					btnDelivered.setEnabled(false);
				}

				lblSubPrice.setText(""
						+ selectedOrder.getTotalPrice().subtract(
								selectedOrder.getSupplier().getShippingCost()));
				lblShippingCost.setText(""
						+ selectedOrder.getSupplier().getShippingCost());
				lblPrice.setText("" + selectedOrder.getTotalPrice());
			}
		});
		scrollPane1.setViewportView(table);

		//create the second scrollpane and set the attributes
		scrollPane2 = new JScrollPane();
		scrollPane2.setBounds(276, 12, 445, 452);
		contentPane.add(scrollPane2);

		//create the order panel and set the attributes
		orderPanel = new JPanel();
		orderPanel.setPreferredSize(new Dimension(430, 450));
		orderPanel.setBackground(Color.WHITE);
		scrollPane2.setViewportView(orderPanel);
		orderPanel.setLayout(null);

		//create the logo label and set the attributes
		lblLogo = new JLabel("");
		lblLogo.setBounds(20, 22, 112, 67);

		//get the logo image and resize it to fit the logo label
		Image image = new ImageIcon(getClass().getResource("/Images/logo.png"))
				.getImage();
		Image newimg = image.getScaledInstance(lblLogo.getWidth(),
				lblLogo.getHeight(), java.awt.Image.SCALE_SMOOTH);
		ImageIcon imageIcon = new ImageIcon(newimg);

		//set the logo image as icon for the logo label
		lblLogo.setIcon(imageIcon);
		orderPanel.add(lblLogo);

		//create the separator and set the attributes
		separator = new JSeparator();
		separator.setBounds(20, 87, 112, 2);
		orderPanel.add(separator);

		//create the header label and set the attributes
		lblHeaderLabel = new JLabel("Aankoop order");
		lblHeaderLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblHeaderLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblHeaderLabel.setBounds(268, 22, 149, 25);
		orderPanel.add(lblHeaderLabel);

		//create the info label and set the attributes
		lblInfoLabel = new JLabel(
				"<html>\r\nDe Hartige Hap<br>\r\nStraat 10<br>\r\n1234AB Breda<br>\r\ntel. 076-1234567");
		lblInfoLabel.setVerticalAlignment(SwingConstants.TOP);
		lblInfoLabel.setBounds(20, 100, 112, 67);
		orderPanel.add(lblInfoLabel);

		//create the third scrollpane and set the attributes
		scrollPane3 = new JScrollPane();
		scrollPane3.setBounds(10, 216, 423, 163);
		orderPanel.add(scrollPane3);

		//create the second table and set the attributes
		table1 = new JTable();
		table1.setModel(new DefaultTableModel(new Object[][] {}, new String[] {
				"ID", "Naam", "Aantal", "Prijs (p.p.)", "Totaal" }) {
			private static final long serialVersionUID = 1L;
			boolean[] columnEditables = new boolean[] { false, false, false,
					false, false };

			@Override
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		scrollPane3.setViewportView(table1);

		//create the product label and set the attributes
		lblProducten = new JLabel("Producten:");
		lblProducten.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblProducten.setBounds(20, 190, 69, 17);
		orderPanel.add(lblProducten);

		//create the supplier label and set the attributes
		lblSupplier = new JLabel("Leverancier-gegevens");
		lblSupplier.setVerticalAlignment(SwingConstants.TOP);
		lblSupplier.setBounds(268, 100, 149, 56);
		orderPanel.add(lblSupplier);

		//create the subtotal label and set the attributes
		lblSubTotal = new JLabel("Totaal (excl. BTW):");
		lblSubTotal.setBounds(12, 390, 112, 14);
		orderPanel.add(lblSubTotal);

		//create the subprice label and set the attributes
		lblSubPrice = new JLabel("");
		lblSubPrice.setHorizontalAlignment(SwingConstants.LEFT);
		lblSubPrice.setBounds(128, 390, 53, 14);
		orderPanel.add(lblSubPrice);

		//create the first shippingcost label and set the attributes
		lblShippingCost1 = new JLabel("Verzendkosten:");
		lblShippingCost1.setBounds(12, 407, 93, 14);
		orderPanel.add(lblShippingCost1);

		//create the second shippingcost label and set the attributes
		lblShippingCost = new JLabel("");
		lblShippingCost.setHorizontalAlignment(SwingConstants.LEFT);
		lblShippingCost.setBounds(128, 407, 53, 14);
		orderPanel.add(lblShippingCost);

		//create the third shippingcost label and set the attributes
		lblShippingCost2 = new JLabel("");
		lblShippingCost2.setBounds(135, 407, 46, 14);
		orderPanel.add(lblShippingCost2);

		//create the first total price label and set the attributes
		lblTotal = new JLabel("Totaal:");
		lblTotal.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTotal.setBounds(12, 424, 39, 14);
		orderPanel.add(lblTotal);

		//create the second total price label and set the attributes
		lblPrice = new JLabel("");
		lblPrice.setHorizontalAlignment(SwingConstants.LEFT);
		lblPrice.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPrice.setBounds(128, 424, 53, 14);
		orderPanel.add(lblPrice);

		//create the status label and set the attributes
		lblStatus = new JLabel("Status");
		lblStatus.setHorizontalAlignment(SwingConstants.LEFT);
		lblStatus.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblStatus.setBounds(299, 424, 134, 14);
		orderPanel.add(lblStatus);

		//create the delivered button and set the attributes
		btnDelivered = new JButton("Afgeleverd");
		btnDelivered.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (table.getSelectedRow() < 0) {
					return;
				}
				int id = (Integer) table.getValueAt(table.getSelectedRow(),
						table.convertColumnIndexToView(table.getColumn("ID")
								.getModelIndex()));
				//set the order status to delivered
				manager.changeState(id);
				for (Order order : orders) {
					if (order.getId() == id) {
						order.setState("Geleverd");
						for (OrderItem orderItem : order.getOrderItems()) {
							manager.updateAmount(orderItem.getId(),
									orderItem.getNum());
						}
						gui.refresh();
					}
				}
				lblStatus.setText("Status: Geleverd");
				lblStatus.setBackground(Color.GREEN);
				btnDelivered.setEnabled(false);
				DefaultTableModel dtm = (DefaultTableModel) table.getModel();
				for (Order order : orders) {
					Object[] list = new Object[] { order.getId(),
							order.getDate(), order.getSupplier().getName(),
							order.getState() };
					dtm.addRow(list);
				}
				table.setModel(dtm);
			}
		});
		btnDelivered.setEnabled(false);
		btnDelivered.setBounds(142, 439, 120, 25);
		contentPane.add(btnDelivered);

		//create the delete button and set the attributes
		btnDelete = new JButton("Verwijder");
		btnDelete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean login = new Login().doLogin(OrderSummaryDialog.this,
						manager, true);
				//delete the order
				if (login) {
					manager.deleteOrder((Integer) table.getValueAt(table
							.getSelectedRow(), table
							.convertColumnIndexToView(table.getColumn("ID")
									.getModelIndex())));
					DefaultTableModel model = (DefaultTableModel) table
							.getModel();
					model.removeRow(table.getSelectedRow());
				}
			}
		});
		btnDelete.setBounds(12, 439, 120, 23);
		contentPane.add(btnDelete);

		load.dispose();
		setVisible(true);

		DefaultTableModel dtm = (DefaultTableModel) table.getModel();
		for (Order order : orders) {
			Object[] list = new Object[] { order.getId(), order.getDate(),
					order.getSupplier().getName(), order.getState() };
			dtm.addRow(list);
		}
		table.setModel(dtm);
	}
}