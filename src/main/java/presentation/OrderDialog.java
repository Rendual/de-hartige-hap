package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import businesslogic.ProductManager;
import domain.OrderItem;

public class OrderDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	//The GUI components
	private JButton btnPlaceOrder;
	private JButton btnUpdate;
	private JPanel contentPane;
	private JLabel lblLogo;
	private JLabel lblHeaderLabel;
	private JLabel lblInfoLabel;
	private JLabel lblPrice;
	private JLabel lblProducten;
	private JLabel lblShippingCost2;
	private JLabel lblShippingCost1;
	private JLabel lblSubPrice;
	private JLabel lblSubTotal;
	private JLabel lblSupplier;
	private JLabel lblTotal;
	private BigDecimal oldval = new BigDecimal(0);
	private List<OrderItem> orderItems;
	private JPanel orderPanel;
	private JScrollPane scrollPane1;
	private JScrollPane scrollPane2;
	private JScrollPane scrollPane3;
	private JSeparator separator;
	private JTable table;
	private JTable table1;

    /**
     * Initializes the fields of this class and creates the GUI
     * @param gui the manage GUI
     * @param parent the JFrame this dialog is displayed on
     * @param manager the product manager
     * @param items the order items
     */
	public OrderDialog(final ManageGUI gui, final JFrame parent, final ProductManager manager,
			List<OrderItem> items) {
		super(parent, "Orders", false);

		//creat the loading screen
		LoadSmall load = new LoadSmall(parent, "Aan het laden...");

		//set the frame attributes
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setSize(737, 502);
		setLocationRelativeTo(parent);

		//create the content pane and set the attributes
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);

		orderItems = items;

		//create the first scrollpane and set the attributes
		scrollPane1 = new JScrollPane();
		scrollPane1.setBounds(12, 12, 250, 413);
		contentPane.add(scrollPane1);

		//create the order table
		table = new JTable();
		
		//set the order table model
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] {
				"ID", "Naam", "Leverancier", "Aantal" }) {
			private static final long serialVersionUID = 1L;
			boolean[] columnEditables = new boolean[] { false, false, false,
					true };

			@Override
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		table.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {

			}

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					try {
						int id = (Integer) table.getValueAt(
								table.getSelectedRow(),
								table.convertColumnIndexToView(table.getColumn(
										"ID").getModelIndex()));
						for (int i = 0; i < table1.getRowCount(); i++) {
							int thisid = (Integer) table.getValueAt(i, table
									.convertColumnIndexToView(table.getColumn(
											"ID").getModelIndex()));
							BigDecimal value = new BigDecimal(
									""
											+ table.getValueAt(
													table.getSelectedRow(),
													table.convertColumnIndexToView(table
															.getColumn("Aantal")
															.getModelIndex())));
							if (id == thisid) {
								table1.setValueAt(value, i, table1
										.convertColumnIndexToView(table1
												.getColumn("Aantal")
												.getModelIndex()));
								changeNumberOfIng(id,
										Integer.parseInt("" + value));
							}
						}
						update();
						oldval = new BigDecimal(""
								+ table.getValueAt(table.getSelectedRow(),
										table.convertColumnIndexToView(table
												.getColumn("Aantal")
												.getModelIndex())));
					} catch (NumberFormatException nfe) {
						setBack(table);
					}
				}
			}

			@Override
			public void keyTyped(KeyEvent e) {
			}
		});
		table.getColumnModel().getColumn(0).setPreferredWidth(40);
		table.getColumnModel().getColumn(3).setPreferredWidth(45);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent evnt) {
				DefaultTableModel dtm = (DefaultTableModel) table1.getModel();
				dtm.setRowCount(0);
				BigDecimal totalprice = new BigDecimal(0).setScale(2,
						RoundingMode.HALF_UP);
				BigDecimal shippcost = new BigDecimal(0);
				OrderItem supplieritem = null;
				for (OrderItem item : orderItems) {
					if (item.getId() == (Integer) table.getValueAt(table
							.getSelectedRow(), table
							.convertColumnIndexToView(table.getColumn("ID")
									.getModelIndex()))) {
						supplieritem = item;
					}
				}
				for (OrderItem item : orderItems) {
					Integer id = item.getId();
					if (item.getSupplier().getId() == supplieritem
							.getSupplier().getId()) {
						DefaultTableModel model = (DefaultTableModel) table1
								.getModel();
						OrderItem product = item;
						shippcost = item.getSupplier().getShippingCost();
						BigDecimal price = item.getPrice();
						totalprice = totalprice.add(price
								.multiply(new BigDecimal(product.getNum())));
						Object[] list = { product.getId(), product.getName(),
								product.getNum(), price, 0

						};
						model.addRow(list);
						for (int i = 0; i < table1.getRowCount(); i++) {
							if (table1.getValueAt(i, table1
									.convertColumnIndexToView(table1.getColumn(
											"ID").getModelIndex())) == id) {
								table1.setValueAt(
										""
												+ price.multiply(new BigDecimal(
														product.getNum())), i,
										table1.convertColumnIndexToView(table1
												.getColumn("Totaal")
												.getModelIndex()));
							}
						}
						lblSupplier.setText("<html>\r\n"
								+ product.getSupplier().getName() + "<br>\r\n"
								+ product.getSupplier().getAddress() + "<br>\r\n"
								+ product.getSupplier().getZipcode()
								+ "<br>\r\n" + product.getSupplier().getPhone()
								+ "<br>\r\n" + product.getSupplier().getEmail()
								+ "</html>");
						lblSupplier.setSize(lblSupplier.getPreferredSize());

					}
				}
				setTotals(totalprice, shippcost);
			}
		});
		scrollPane1.setViewportView(table);

		//create the second scrollpane and set the attributes
		scrollPane2 = new JScrollPane();
		scrollPane2.setBounds(276, 12, 445, 452);
		contentPane.add(scrollPane2);

		//create the order panel and set the attributes
		orderPanel = new JPanel();
		orderPanel.setPreferredSize(new Dimension(430, 450));
		orderPanel.setBackground(Color.WHITE);
		scrollPane2.setViewportView(orderPanel);
		orderPanel.setLayout(null);

		//create the logo label and set the attributes
		lblLogo = new JLabel("");
		lblLogo.setBounds(20, 22, 112, 67);

		//get the logo image and resize it to fit the logo label
		Image image = new ImageIcon(getClass().getResource("/Images/logo.png"))
				.getImage();
		Image newimg = image.getScaledInstance(lblLogo.getWidth(),
				lblLogo.getHeight(), java.awt.Image.SCALE_SMOOTH);
		ImageIcon imageIcon = new ImageIcon(newimg);

		//set the logo image as icon for the logo label
		lblLogo.setIcon(imageIcon);
		orderPanel.add(lblLogo);

		//create the separator and set the attributes
		separator = new JSeparator();
		separator.setBounds(20, 87, 112, 2);
		orderPanel.add(separator);

		//create the header label and set the attributes
		lblHeaderLabel = new JLabel("Aankoop order");
		lblHeaderLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblHeaderLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblHeaderLabel.setBounds(268, 22, 149, 25);
		orderPanel.add(lblHeaderLabel);

		//create the info label and set the attributes
		lblInfoLabel = new JLabel(
				"<html>\r\nDe Hartige Hap<br>\r\nStraat 10<br>\r\n1234AB Breda<br>\r\ntel. 076-1234567");
		lblInfoLabel.setVerticalAlignment(SwingConstants.TOP);
		lblInfoLabel.setBounds(20, 100, 112, 67);
		orderPanel.add(lblInfoLabel);

		//create the third scrollpane and set the attributes
		scrollPane3 = new JScrollPane();
		scrollPane3.setBounds(10, 216, 423, 163);
		orderPanel.add(scrollPane3);

		//create the second table and set the attributes
		table1 = new JTable();
		table1.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					int row = table1.getSelectedRow();
					if (row < 0) {
						return;
					}
					try {
						BigDecimal pricepp = (BigDecimal) table1.getValueAt(
								row, table1.convertColumnIndexToView(table1
										.getColumn("Prijs (p.p.)")
										.getModelIndex()));
						table1.setValueAt(
								""
										+ (pricepp
												.multiply(new BigDecimal(
														""
																+ table1.getValueAt(
																		row,
																		table1.convertColumnIndexToView(table1
																				.getColumn(
																						"Aantal")
																				.getModelIndex()))))),
								row, table1.convertColumnIndexToView(table1
										.getColumn("Totaal").getModelIndex()));
						BigDecimal value = new BigDecimal(""
								+ table1.getValueAt(row, table1
										.convertColumnIndexToView(table1
												.getColumn("Aantal")
												.getModelIndex())));
						for (int i = 0; i < table.getRowCount(); i++) {
							if (table.getValueAt(i, table
									.convertColumnIndexToView(table.getColumn(
											"ID").getModelIndex())) == table1
									.getValueAt(row, table1
											.convertColumnIndexToView(table1
													.getColumn("ID")
													.getModelIndex()))) {
								table.setValueAt(value, i, table
										.convertColumnIndexToView(table
												.getColumn("Aantal")
												.getModelIndex()));
							}
						}
						changeNumberOfIng((Integer) table1.getValueAt(row,
								table1.convertColumnIndexToView(table1
										.getColumn("ID").getModelIndex())),
								Integer.parseInt("" + value));
						BigDecimal pricetotal = new BigDecimal(0).setScale(2,
								RoundingMode.HALF_UP);
						for (int i = 0; i < table1.getRowCount(); i++) {
							pricetotal = pricetotal.add(new BigDecimal(""
									+ table1.getValueAt(i, table1
											.convertColumnIndexToView(table1
													.getColumn("Totaal")
													.getModelIndex()))));
						}
						setTotals(pricetotal,
								new BigDecimal(lblShippingCost2.getText()));
						oldval = new BigDecimal(""
								+ table1.getValueAt(row, table1
										.convertColumnIndexToView(table1
												.getColumn("Aantal")
												.getModelIndex())));
					} catch (NumberFormatException nfe) {
						setBack(table1);
					}
				}
			}

			@Override
			public void keyTyped(KeyEvent e) {
			}
		});
		
		//set the second table model
		table1.setModel(new DefaultTableModel(new Object[][] {}, new String[] {
				"ID", "Naam", "Aantal", "Prijs (p.p.)", "Totaal" }) {

			private static final long serialVersionUID = 1L;

			boolean[] columnEditables = new boolean[] { false, false, true,
					false, false };

			@Override
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		scrollPane3.setViewportView(table1);

		//create the product label and set the attributes
		lblProducten = new JLabel("Producten:");
		lblProducten.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblProducten.setBounds(20, 190, 69, 17);
		orderPanel.add(lblProducten);

		//create the supplier label and set the attributes
		lblSupplier = new JLabel("Leverancier-gegevens");
		lblSupplier.setVerticalAlignment(SwingConstants.TOP);
		lblSupplier.setBounds(268, 100, 149, 56);
		orderPanel.add(lblSupplier);

		//create the subtotal label and set the attributes
		lblSubTotal = new JLabel("Totaal (excl. BTW):");
		lblSubTotal.setBounds(12, 390, 112, 14);
		orderPanel.add(lblSubTotal);

		//create the subprice label and set the attributes
		lblSubPrice = new JLabel("");
		lblSubPrice.setHorizontalAlignment(SwingConstants.LEFT);
		lblSubPrice.setBounds(128, 390, 53, 14);
		orderPanel.add(lblSubPrice);

		//create the first shipping cost label and set the attributes
		lblShippingCost1 = new JLabel("Verzendkosten:");
		lblShippingCost1.setBounds(12, 407, 93, 14);
		orderPanel.add(lblShippingCost1);

		//create the second shopping cost label and set the attributes
		lblShippingCost2 = new JLabel("");
		lblShippingCost2.setHorizontalAlignment(SwingConstants.LEFT);
		lblShippingCost2.setBounds(128, 407, 53, 14);
		orderPanel.add(lblShippingCost2);

		//create the first total price label and set the attributes
		lblTotal = new JLabel("Totaal:");
		lblTotal.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTotal.setBounds(12, 424, 39, 14);
		orderPanel.add(lblTotal);

		//create the second total price label and set the attributes
		lblPrice = new JLabel("");
		lblPrice.setHorizontalAlignment(SwingConstants.LEFT);
		lblPrice.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPrice.setBounds(128, 424, 53, 14);
		orderPanel.add(lblPrice);

		//create the place order button and set the attributes
		btnPlaceOrder = new JButton("Plaats order(s)");
		btnPlaceOrder.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//save the order
				manager.saveOrder(orderItems);
				
				//open the order summary dialog
				new OrderSummaryDialog(gui, parent, manager);
				dispose();
			}
		});
		btnPlaceOrder.setBounds(142, 439, 120, 25);
		contentPane.add(btnPlaceOrder);

		//create the update button and set the attributes
		btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				update();
			}
		});
		btnUpdate.setBounds(12, 439, 120, 23);
		contentPane.add(btnUpdate);

		load.dispose();

		setVisible(true);

		//fill the table
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		for (OrderItem orderItem : items) {
			Object[] list = { orderItem.getId(), orderItem.getName(),
					orderItem.getSupplier().getName(), orderItem.getNum() };
			model.addRow(list);
		}
		table.setModel(model);
	}
	
	/**
	 * Updates the amount of  a product
	 * @param id the product id
	 * @param number the new amount
	 */
	private void changeNumberOfIng(int id, int number) {
		for (OrderItem item : orderItems) {
			if (item.getId() == id) {
				item.setNum(number);
			}
		}
	}

	/**
	 * Sets the values in a table back to their old values
	 * @param table the table
	 */
	private void setBack(JTable table) {
		table.setValueAt(
				(oldval.compareTo(BigDecimal.ZERO) <= 0) ? BigDecimal.ONE
						: oldval, table.getSelectedRow(), table
						.convertColumnIndexToView(table.getColumn("Aantal")
								.getModelIndex()));
		JOptionPane.showMessageDialog(OrderDialog.this,
				"U heeft een verkeerde waarde ingevuld!");
	}

	/**
	 * Displays the total prices
	 * @param totalPrice the total price
	 * @param shippingCost the shipping cost
	 */
	private void setTotals(BigDecimal totalPrice, BigDecimal shippingCost) {
		lblSubPrice.setText("" + totalPrice);
		lblShippingCost2.setText("" + shippingCost);
		lblPrice.setText("" + totalPrice.add(shippingCost));
	}

	/**
	 * Updates the prices
	 */
	private void update() {
		BigDecimal totalprice = new BigDecimal(0).setScale(2,
				RoundingMode.HALF_UP);
		for (int i = 0; i < table1.getRowCount(); i++) {
			BigDecimal count = new BigDecimal(""
					+ table1.getValueAt(
							i,
							table1.convertColumnIndexToView(table1.getColumn(
									"Aantal").getModelIndex())));
			BigDecimal pricepp = new BigDecimal(""
					+ table1.getValueAt(
							i,
							table1.convertColumnIndexToView(table1.getColumn(
									"Prijs (p.p.)").getModelIndex())));
			BigDecimal total = pricepp.multiply(count);
			table1.setValueAt(total, i, table1.convertColumnIndexToView(table1
					.getColumn("Totaal").getModelIndex()));
			totalprice = totalprice.add(total);
		}
		setTotals(totalprice, new BigDecimal(lblShippingCost2.getText()));
	}
}