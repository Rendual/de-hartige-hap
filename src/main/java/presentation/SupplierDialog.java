package presentation;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.Spring;
import javax.swing.SpringLayout;
import javax.swing.SpringLayout.Constraints;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import businesslogic.ProductManager;
import domain.Supplier;

public class SupplierDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	//The GUI components
	private JButton btnOpslaan;
	private JButton btnNewSupplier;
	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JTable table;

    /**
     * Initializes the fields of this class and creates the GUI
     * @param parent the JFrame this dialog is displayed on
     * @param manager the product manager
     */
	public SupplierDialog(final JFrame parent, final ProductManager manager) {
		super(parent, "Leveranciersbeheer", false);

		//create the loading screen
		LoadSmall load = new LoadSmall(parent, "Aan het laden...");

		//set the frame attributes
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setSize(662, 341);
		setLocationRelativeTo(parent);

		//create the content pane and set the attributes
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);

		//create the srollpane and set the attributes
		scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 12, 632, 250);
		contentPane.add(scrollPane);

		//create the supplier table and set the attributes
		table = new JTable();
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] {
				"ID", "Naam", "Adres", "Postcode", "Telefoon", "Email",
				"Verzendkosten" }) {

			private static final long serialVersionUID = 1L;

			boolean[] columnEditables = new boolean[] { false, true, true,
					true, true, true, true };

			@Override
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		table.getColumnModel().getColumn(0).setPreferredWidth(40);
		DefaultTableModel dtm = (DefaultTableModel) table.getModel();
		dtm.setRowCount(0);
		for (Supplier supplier : manager.getSuppliers()) {
			Object[] list = { supplier.getId(), supplier.getName(),
					supplier.getAddress(), supplier.getZipcode(),
					supplier.getPhone(), supplier.getEmail(),
					supplier.getShippingCost() };
			dtm.addRow(list);
		}
		scrollPane.setViewportView(table);

		//create the save button and set the attributes
		btnOpslaan = new JButton("Opslaan");
		btnOpslaan.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				for (int i = 0; i < table.getRowCount(); i++) {
					int id = (Integer) table.getValueAt(i, table
							.convertColumnIndexToView(table.getColumn("ID")
									.getModelIndex()));
					String name = (String) table.getValueAt(
							i,
							table.convertColumnIndexToView(table.getColumn(
									"Naam").getModelIndex()));
					String address = (String) table.getValueAt(
							i,
							table.convertColumnIndexToView(table.getColumn(
									"Adres").getModelIndex()));
					String zipcode = (String) table.getValueAt(
							i,
							table.convertColumnIndexToView(table.getColumn(
									"Postcode").getModelIndex()));
					String phone = (String) table.getValueAt(
							i,
							table.convertColumnIndexToView(table.getColumn(
									"Telefoon").getModelIndex()));
					String email = (String) table.getValueAt(
							i,
							table.convertColumnIndexToView(table.getColumn(
									"Email").getModelIndex()));
					BigDecimal shippingCost = new BigDecimal(""
							+ table.getValueAt(i, table
									.convertColumnIndexToView(table.getColumn(
											"Verzendkosten").getModelIndex())));
					
					//check which syppliers have been updated
					boolean needsUpdate = false;
					for (Supplier supplier : manager.getSuppliers()) {
						if (supplier.getId() == id && (!supplier.getName().equals(name)
								|| !supplier.getAddress().equals(address)
								|| !supplier.getZipcode().equals(zipcode)
								|| !supplier.getPhone().equals(phone)
								|| !supplier.getEmail().equals(email)
								|| !supplier.getShippingCost().equals(shippingCost))) {
							needsUpdate = true;
						}
					}
					
					//update the suppliers
					if (needsUpdate) {
						manager.saveSupplier(new Supplier(id, name, address, zipcode,
								phone, email, shippingCost));
					}
				}
				dispose();
			}
		});
		btnOpslaan.setBounds(549, 276, 97, 25);
		contentPane.add(btnOpslaan);
		
		//create the bew supplier button and set the attributes
		btnNewSupplier = new JButton("Nieuwe leverancier");
		btnNewSupplier.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//create the new supplier form
				JLabel lblName = new JLabel("Naam:");
				JTextField tfName = new JTextField(15);
				JLabel lblAdres = new JLabel("Adres:");
				JTextField tfAdres = new JTextField(15);
				JLabel lblZipcode = new JLabel("Postcode:");
				JTextField tfZipcode = new JTextField(15);
				JLabel lblPhone = new JLabel("Telefoon:");
				JTextField tfPhone = new JTextField(15);
				JLabel lblEmail = new JLabel("Email:");
				JTextField tfEmail = new JTextField(15);
				JLabel lblShippingCost = new JLabel("Verzendkosten:");
				JTextField tfShippingCost = new JTextField(15);

				JPanel supplierPanel = new JPanel(new SpringLayout());

				supplierPanel.add(lblName);
				supplierPanel.add(tfName);
				supplierPanel.add(lblAdres);
				supplierPanel.add(tfAdres);
				supplierPanel.add(lblZipcode);
				supplierPanel.add(tfZipcode);
				supplierPanel.add(lblPhone);
				supplierPanel.add(tfPhone);
				supplierPanel.add(lblEmail);
				supplierPanel.add(tfEmail);
				supplierPanel.add(lblShippingCost);
				supplierPanel.add(tfShippingCost);

				makeCompactGrid(supplierPanel, 6, 2, 6, 6, 6, 6);

				int result = JOptionPane.showConfirmDialog(null, supplierPanel,
						"", JOptionPane.OK_CANCEL_OPTION);

				if (result == JOptionPane.OK_OPTION) {
					//add the new supplier
					LoadSmall load = new LoadSmall(parent, "Aan het updaten...");
					manager.addSupplier(new Supplier(0, tfName.getText(),
							tfAdres.getText(), tfZipcode.getText(), tfPhone
									.getText(), tfEmail.getText(),
							new BigDecimal(tfShippingCost.getText())));
					DefaultTableModel dtm = (DefaultTableModel) table.getModel();
					dtm.setRowCount(0);
					for (Supplier supplier : manager.getSuppliers()) {
						Object[] list = { supplier.getId(), supplier.getName(),
								supplier.getAddress(), supplier.getZipcode(),
								supplier.getPhone(), supplier.getEmail(),
								supplier.getShippingCost() };
						dtm.addRow(list);
					}
					load.dispose();
				}
			}
		});
		btnNewSupplier.setBounds(412, 276, 123, 25);
		contentPane.add(btnNewSupplier);

		load.dispose();

		setVisible(true);
	}
	
	/**
	 * Creates constraints for a cel 
	 * @param row the row
	 * @param col the column
	 * @param parent the parent Container to display the constraints on
	 * @param cols the amount of columns
	 * @return the constracts as a Contraints object
	 */
	private Constraints getConstraintsForCell(int row, int col,
			Container parent, int cols) {
		SpringLayout layout = (SpringLayout) parent.getLayout();
		Component c = parent.getComponent(row * cols + col);
		return layout.getConstraints(c);
	}
	
	/**
	 * Creates a compact grid
	 * @param parent the parent Container to display this grid on
	 * @param rows the amount of rows
	 * @param cols the amount of columns
	 * @param initialX the initial x value
	 * @param initialY the initial y value
	 * @param xPad the x padding
	 * @param yPad the y padding
	 */
	private void makeCompactGrid(Container parent, int rows, int cols,
			int initialX, int initialY, int xPad, int yPad) {
		SpringLayout layout;
		try {
			layout = (SpringLayout) parent.getLayout();
		} catch (ClassCastException exc) {
			return;
		}

		Spring x = Spring.constant(initialX);
		for (int c = 0; c < cols; c++) {
			Spring width = Spring.constant(0);
			for (int r = 0; r < rows; r++) {
				width = Spring.max(width,
						getConstraintsForCell(r, c, parent, cols).getWidth());
			}
			for (int r = 0; r < rows; r++) {
				SpringLayout.Constraints constraints = getConstraintsForCell(r,
						c, parent, cols);
				constraints.setX(x);
				constraints.setWidth(width);
			}
			x = Spring.sum(x, Spring.sum(width, Spring.constant(xPad)));
		}

		Spring y = Spring.constant(initialY);
		for (int r = 0; r < rows; r++) {
			Spring height = Spring.constant(0);
			for (int c = 0; c < cols; c++) {
				height = Spring.max(height,
						getConstraintsForCell(r, c, parent, cols).getHeight());
			}
			for (int c = 0; c < cols; c++) {
				SpringLayout.Constraints constraints = getConstraintsForCell(r,
						c, parent, cols);
				constraints.setY(y);
				constraints.setHeight(height);
			}
			y = Spring.sum(y, Spring.sum(height, Spring.constant(yPad)));
		}

		SpringLayout.Constraints pCons = layout.getConstraints(parent);
		pCons.setConstraint(SpringLayout.SOUTH, y);
		pCons.setConstraint(SpringLayout.EAST, x);
	}
}