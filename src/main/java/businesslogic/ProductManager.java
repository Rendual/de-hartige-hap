package businesslogic;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import datastorage.LoginDAO;
import datastorage.ProductDAO;
import domain.LogItem;
import domain.Order;
import domain.OrderItem;
import domain.ProdType;
import domain.ProdUnit;
import domain.Product;
import domain.Supplier;

public class ProductManager {
	
	//The currently logged in user
	private String user;
	
	//The product DAO
	private ProductDAO productDAO;
	
	//The login DAO
	private LoginDAO loginDAO;
	
    /**
     * Initializes the fields of this class
     */
	public ProductManager() {
		this.user = "";
		this.productDAO = new ProductDAO();
		this.loginDAO = new LoginDAO();
	}

    /**
     * Retrieves the product list
     * @param title the search title
     * @param order the order to return the list in
     * @return the product list as a List of Product
     */
	public List<Product> giveProductList(String title, String order) {
		return productDAO.giveProductList(title, order);
	}

    /**
     * Retrieves a single product
     * @param id the product id
     * @return the product as a Product object
     */
	public Product giveProduct(int id) {
		return productDAO.giveProduct(id);
	}

    /**
     * Updates a product
     * @param product the product to update
     */
	public void updateProduct(Product product) {
		productDAO.updateProduct(product);
		productDAO.doLog(user, "Update product: " + product.getTitle());
	}

    /**
     * Deletes a product
     * @param id the product id
     * @param title the product title
     */
	public void deleteProduct(int id, String title) {
		productDAO.deleteProduct(id);
		productDAO.doLog(user, "Delete product: " + title);
	}

    /**
     * Adds a new product
     * @param title the product title
     * @param description the product description
     * @param stock the current stock
     * @param unit the product unit
     * @param type the product type
     * @param min the minimum stock
     * @param supplier the product supplier
     * @param price the product price
     * @throws java.sql.SQLException if an error occurred whie trying to insert the product
     */
	public void insertProduct(String title, String description, int stock,
			ProdUnit unit, ProdType type, int min, Supplier supplier, BigDecimal price) throws SQLException {
		productDAO.insertProduct(title, description, stock, unit, type, min, supplier, price);
		productDAO.doLog(user, "Insert product: " + title);
	}

    /**
     * Retrieves the product types
     * @return the product types as a List of ProdType
     */
	public List<ProdType> getTypes() {
		return productDAO.getTypes();
	}

    /**
     * Retrieves the product units
     * @return the product units as a List of ProdUnit
     */
	public List<ProdUnit> getUnits() {
		return productDAO.getUnits();
	}
	
    /**
     * Retrieves the log items
     * @param order  the order to return the list in
     * @return the log items as a List of LogItem
     */
	public List<LogItem> getLog(String order) {
		return productDAO.getLog(order);
	}
	
    /**
     * Retrieves the suppliers
     * @return the suppliers a a List of Supplier
     */
	public List<Supplier> getSuppliers() {
		return productDAO.getSuppliers();
	}
	
    /**
     * Retrieves the supplier for a product
     * @param id the produt id
     * @return the supplier as a Supplier object
     */
	public Supplier getSupplierFromProd(int id) {
		return productDAO.getSupplierFromProd(id);
	}
	
    /**
     * Retrieves the suppliers per product
     * @param items the list of items
     * @return the suppliers per product as a HashMap of two Integers
     */
	public HashMap<Integer, Integer> getSupplierProduct(List<OrderItem> items) {
		return productDAO.getSupplierProduct((ArrayList<OrderItem>) items);
	}
	
    /**
     * Retrieves the price and shipping cost for a product
     * @param prodId the product id
     * @param supId the supplier id
     * @return the price and shipping cost as a BigDecimal array
     */
	public BigDecimal[] getPriceShippCost(int prodId, int supId) {
		return productDAO.getPriceShippCost(prodId, supId);
	}
	
    /**
     * Saves an order
     * @param items the order items
     */
	public void saveOrder(List<OrderItem> items) {
		List<Order> orders = new ArrayList<Order>();
		for(OrderItem item : items) {
			boolean isAdded = false;
			for(Order order : orders) {
				//check if there is already an order for this supplier
				if(item.getSupplier().getId() == order.getSupplier().getId()) {
					//if there is add the item to the existing order
					order.addOrderItem(item);
					isAdded = true;
				}
			}
			//check if there was already an existing order
			if(!isAdded) {
				//if not create a new order
				Order newOrder = new Order(item.getSupplier());
				newOrder.addOrderItem(item);
				orders.add(newOrder);
			}		
		}
		for(Order order : orders) {
			//calculate the total price of the order
			order.calculateTotalPrice();
			
			//save the order
			int newId = productDAO.saveOrder(order.getSupplier().getId(), order.getTotalPrice().setScale(2, RoundingMode.HALF_UP).add(order.getSupplier().getShippingCost()));
			for(OrderItem orderItem : order.getOrderItems()) {
				//save the items that belong to the order
				productDAO.saveOrderItem(orderItem.getId(), orderItem.getNum(), newId);
			}
		}
	}
	
    /**
     * Retrieves the orders
     * @return the orders as a List of Order
     */
	public List<Order> getOrders() {
		//retrieve the orders
		List<Order> orders = productDAO.getOrders();
		for(Order order : orders) {
			//set the supplier for this order
			order.setSupplier(productDAO.getSupplierByOrder(order.getId()));
			for(OrderItem orderItem : productDAO.getOrderItemsByOrder(order.getId())) {
				//set the items for this order
				order.addOrderItem(orderItem);
			}
		}
		return orders;
	}
	
    /**
     * Saves a supplier
     * @param supplier the supplier
     */
	public void saveSupplier(Supplier supplier) {
		productDAO.saveSupplier(supplier);
		productDAO.doLog(user, "Update supplier: "+supplier.getName());
	}
	
    /**
     * Adds a supplier
     * @param supplier the supplier
     */
	public void addSupplier(Supplier supplier) {
		productDAO.addSupplier(supplier);
		productDAO.doLog(user, "Insert supplier: "+supplier.getName());
	}
	
    /**
     * Adds a product unit
     * @param unit the product unit
     */
	public void addUnit(ProdUnit unit) {
		productDAO.addUnit(unit);
		productDAO.doLog(user, "Insert unit: "+unit.getName());
	}
	
    /**
     * Adds a product type
     * @param type the product type
     */
	public void addType(ProdType type) {
		productDAO.addType(type);
		productDAO.doLog(user, "Insert type: "+type.getName());
	}
	
    /**
     * Updates an image
     * @param file the image file
     * @param id the image id
     */
	public void updateImage(File file, int id) {
		productDAO.updateImage(file, id);
		productDAO.doLog(user, "Update image: "+file.getName());
	}
	
    /**
     * Adds an image
     * @param file the image file
     * @return the new image id as an int
     */
	public int setImage(File file) {
		return productDAO.setImage(file);
	}
	
    /**
     * Updates the price of a product
     * @param price the new product price
     * @param prodId the product id
     */
	public void updatePrice(BigDecimal price, int prodId) {
		productDAO.updatePrice(price, prodId);
	}
	
    /**
     * Retrieves the image id for a product
     * @param id the product id
     * @return the image id as an int
     */
	public int getImageId(int id) {
		return productDAO.getImageId(id);
	}
	
    /**
     * Changes the state of an order
     * @param id the order id
     */
	public void changeState(int id) {
		productDAO.changeState(id);
		productDAO.doLog(user, "Update order state: "+id);
	}
	
    /**
     * Updates the stock of a product
     * @param id the product id
     * @param amount the new stock amount
     * @return the result as a boolean
     */
	public boolean updateAmount(int id, int amount) {
		return productDAO.updateAmount(user, id, amount);
	}
	
    /**
     * Checks whether an employee login is valid
     * @param barcode the employee's barcode
     * @param manager a flag to determine whether an employee is a manager
     * @return the login result as a boolean
     */
	public boolean checkLogin(int barcode, boolean manager) {
		//verify the login
		String user = loginDAO.checkLogin(barcode, manager);
		//if the result isn't empty the login was successful
		if (!user.isEmpty()) {
			this.user = user;
			return true;
		}
		return false;
	}
	
    /**
     * Truncates the log
     */
	public void truncateLog() {
		productDAO.truncateLog();
		productDAO.doLog(user, "Truncate log");
	}
	
    /**
     * Deletes an orde
     * @param id the order id
     */
	public void deleteOrder(int id) {
		productDAO.deleteOrder(id);
		productDAO.doLog(user, "Delete order: "+id);
	}
}