package datastorage;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class DatabaseConnection {
	
	//The logger instance
	private Logger logger = Logger.getLogger(DatabaseConnection.class.getName());

	//The database connection
	private Connection connection;

	//The statement on which queries are executed
	private Statement statement;
	
	//The current database user
	private String dbUser;
	
	//The current database password
	private String dbPass;

    /**
     * Initializes the fields of this class
     */
	public DatabaseConnection() {
		connection = null;
		statement = null;
		dbUser = "MagazijnmedC4";
		dbPass = "maginkc4";
	}

    /**
     * Set the database user for this connection
     * @param dbUser the new database user
     */
	public void setDBUser(String dbUser) {
		this.dbUser = dbUser;
	}

    /**
     * Opens a new connection to the database
     * @return the result as a boolean
     */
	public boolean openConnection() {
		boolean result = false;

		//only attempts connecting if there is currently no connection set
		if (connection == null) {
			try {
				//attempt to connect to the database
				connection = DriverManager.getConnection(
						"jdbc:mysql://mysql.famcoolen.nl/avans_hartigehap_oplevering", dbUser, dbPass);

				//set the statement if we successfully connected
				if (connection != null) {
					statement = connection.createStatement();
				}

				result = true;
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(new JFrame(), "Er is geen verbinding met de database, controleer uw netwerkverbinding!");
			}
		} else {
			result = true;
		}
		return result;
	}

    /**
     * Checks if the connection to the database is open
     * @return the result as a boolean
     */
	public boolean connectionIsOpen() {
		boolean open = false;

		//check if the connection and statement are set
		if (connection != null && statement != null) {
			try {
				//check if the connection and statement are open
				open = !connection.isClosed() && !statement.isClosed();
			} catch (SQLException e) {
				logger.log(Level.SEVERE, e.getMessage());
				open = false;
			}
		}
		return open;
	}

    /**
     * Closes the connection to the database
     */
	public void closeConnection() {
		try {
			statement.close();
			connection.close();
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}
	}

    /**
     * Executes an SQL select statement
     * @param query the query string
     * @return the query result as a ResultSet
     */
	public ResultSet executeSQLSelectStatement(String query) {
		ResultSet resultset = null;

		//check if there is a valid query and connection
		if (query != null && connectionIsOpen()) {
			try {
				//execute the query
				resultset = statement.executeQuery(query);
			} catch (SQLException e) {
				logger.log(Level.SEVERE, e.getMessage());
				resultset = null;
			}
		}
		return resultset;
	}

    /**
     * Executes an SQL delete statement
     * @param query the query string
     * @return the query result as a boolean
     */
	public boolean executeSQLDeleteStatement(String query) {
		boolean result = false;

		//check if there is a valid query and connection
		if (query != null && connectionIsOpen()) {
			try {
				//execute the query
				statement.executeUpdate(query);
				result = true;
			} catch (SQLException e) {
				logger.log(Level.SEVERE, e.getMessage());
				result = false;
			}
		}
		return result;
	}

    /**
     * Executes an SQL insert statement
     * @param query the query string
     * @return the query result as a boolean
     */
	public boolean executeSQLInsertStatement(String query) {
		boolean result = false;

		//check if there is a valid query and connection
		if (query != null && connectionIsOpen()) {
			try {
				//execute the query
				statement.execute(query);
				result = true;
			} catch (SQLException e) {
				logger.log(Level.SEVERE, e.getMessage());
				result = false;
			}
		}
		return result;
	}

    /**
     * Executes an SQL update statement
     * @param query the query string
     * @return the query result as a boolean
     */
	public boolean executeSQLUpdateStatement(String query) {
		boolean result = false;

		//check if there is a valid query and connection
		if (query != null && connectionIsOpen()) {
			try {
				//execute the query
				statement.executeUpdate(query);
				result = true;
			} catch (SQLException e) {
				logger.log(Level.SEVERE, e.getMessage());
				result = false;
			}
		}
		return result;
	}
	
    /**
     * Adds an image to the database using a prepared statement
     * @param type the image type
     * @param fis the file input stream for the image
     * @param sizefs the size of the file input stream
     * @param size the image size
     * @param name the image name
     * @return the query result as a boolean
     */
	public boolean executeAddImage(String type, FileInputStream
			fis, int sizefs, String size, String name) {
		boolean result = false;

		//check if there is a valid connection
		if (connectionIsOpen()) {
			try {
				//create and execute a prepared statement
				PreparedStatement ps = connection.prepareStatement("UPDATE `images` SET type = ?, image = ?, size = ?, name = ? WHERE image_id = LAST_INSERT_ID()");
				ps.setString(1, type);
				ps.setBinaryStream(2, fis, sizefs);
				ps.setString(3, size);
				ps.setString(4, name);
				ps.execute();
				result = true;
			} catch (SQLException e) {
				logger.log(Level.SEVERE, e.getMessage());
				result = false;
			}
		}
		return result;
	}
	
    /**
     * updates an image in the database using a prepared statement
     * @param type the image type
     * @param fis the file input stream for the image
     * @param sizefs the size of the file input stream
     * @param size the image size
     * @param name the image name
     * @param id the image id
     * @return the query result as a boolean
     */
	public boolean executeUpdateImage(String type, FileInputStream
			fis, int sizefs, String size, String name, int id) {
		boolean result = false;

		//check if there is a valid connection
		if (connectionIsOpen()) {
			try {
				//create and execute a prepared statement
				PreparedStatement ps = connection.prepareStatement("UPDATE `images` SET type = ?, image = ?, size = ?, name = ? WHERE image_id = "+id);
				ps.setString(1, type);
				ps.setBinaryStream(2, fis, sizefs);
				ps.setString(3, size);
				ps.setString(4, name);
				ps.execute();
				result = true;
			} catch (SQLException e) {
				logger.log(Level.SEVERE, e.getMessage());
				result = false;
			}
		}
		return result;
	}
}