package datastorage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;

import domain.LogItem;
import domain.Order;
import domain.OrderItem;
import domain.ProdType;
import domain.ProdUnit;
import domain.Product;
import domain.Supplier;

public class ProductDAO {
	
	//The logger instance
	private Logger logger = Logger.getLogger(ProductDAO.class.getName());

    /**
     * Checks whether an employee login is valid
     * @param title the search title
     * @param order the order to return the list in
     * @return the product list as a List of Product
     */
	public List<Product> giveProductList(String title, String order) {
		List<Product> productList = new ArrayList<Product>();
		
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			ResultSet resultset = null;
			//execute a select statement
			resultset = connection
					.executeSQLSelectStatement("SELECT ingredient_id, ingredient_name, stock, min FROM viewingredients WHERE ingredient_name LIKE '%"
							+ title
							+ "%' OR description LIKE '%"
							+ title
							+ "%' " + order);
			//check if there is a valid query result
			if (resultset != null) {
				try {
					while (resultset.next()) {
						//get the product info and create the product
						int idRes = resultset.getInt("ingredient_id");
						String titleRes = resultset
								.getString("ingredient_name");
						int stock = resultset.getInt("stock");
						int min = resultset.getInt("min");
						Product newProduct = new Product(idRes, titleRes, "",
								null, stock, null, min, null, null);
						productList.add(newProduct);
					}
				} catch (SQLException e) {
					logger.log(Level.SEVERE, e.getMessage());
				}
			}
			//close the database connection
			connection.closeConnection();
		}
		return productList;
	}

    /**
     * Retrieves a single product
     * @param id the product id
     * @return the product as a Product object
     */
	public Product giveProduct(int id) {
		Product product = null;

		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			ResultSet resultset = null;
			//execute a select statement
			resultset = connection
					.executeSQLSelectStatement("SELECT * FROM viewingredients WHERE ingredient_id = "
							+ id);
			//check if there is a valid query result
			if (resultset != null) {
				try {
					while (resultset.next()) {
						//get the product info and create the product
						int idRes = resultset.getInt("ingredient_id");
						String titleRes = resultset.getString("ingredient_name");
						String description = resultset.getString("description");
						ImageIcon image = getImage(resultset
								.getInt("ingredient_id"));
						int stock = resultset.getInt("stock");
						ProdUnit unit = new ProdUnit(resultset.getInt("unit"),
								resultset.getString("unit_name"));
						int min = resultset.getInt("min");
						ProdType type = new ProdType(resultset.getInt("type"),
								resultset.getString("type_name"));
						Supplier supplier = new Supplier(
								resultset.getInt("supplier_id"),
								resultset.getString("supplier_name"),
								resultset.getString("adres"),
								resultset.getString("zipcode"),
								resultset.getString("phone"),
								resultset.getString("email"),
								resultset.getBigDecimal("shippingcost"));
						product = new Product(idRes, titleRes, description, image,
								stock, unit, min, type, supplier);
					}
				} catch (SQLException e) {
					logger.log(Level.SEVERE, e.getMessage());
				}
			}
			//close the database connection
			connection.closeConnection();
		}
		return product;
	}

    /**
     * Updates a product
     * @param product the product to update
     */
	public void updateProduct(Product product) {
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			//execute update statements
			connection
					.executeSQLUpdateStatement("UPDATE ingredient SET name = '"
							+ product.getTitle() + "', stock = "
							+ product.getStock() + ", unit = "
							+ product.getUnit().getId() + ", min = "
							+ product.getMin() + ", description = '"
							+ product.getDescription() + "', type = "
							+ product.getType().getValue() + ", supplier = "
							+ product.getSupplier().getId()
							+ " WHERE ingredient_id = " + product.getId());
			connection
					.executeSQLUpdateStatement("UPDATE product_supplier SET supplier_id = "
							+ product.getSupplier().getId()
							+ " WHERE product_id = " + product.getId());
			//close the database connection
			connection.closeConnection();
		}
	}

    /**
     * Deletes a product
     * @param id the product id
     */
	public void deleteProduct(int id) {
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			//execute delete statements
			connection
					.executeSQLDeleteStatement("DELETE FROM `product_supplier` WHERE product_id = "
							+ id);
			connection
					.executeSQLDeleteStatement("DELETE FROM `ingredient` WHERE ingredient_id = "
							+ id);
			connection
					.executeSQLDeleteStatement("DELETE FROM `images` WHERE image_id = "
							+ id);
			//close the database connection
			connection.closeConnection();
		}
	}

    /**
     * Adds a new product
     * @param title the product title
     * @param description the product description
     * @param stock the current stock
     * @param unit the product unit
     * @param type the product type
     * @param min the minimum stock
     * @param supplier the product supplier
     * @param price the product price
     */
	public void insertProduct(String title, String description, int stock,
			ProdUnit unit, ProdType type, int min, Supplier supplier,
			BigDecimal price) {
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			//execute insert statements
			connection
					.executeSQLInsertStatement("INSERT INTO `ingredient` VALUES (NULL, '"
							+ title
							+ "', "
							+ stock
							+ ", "
							+ unit.getId()
							+ ", '"
							+ description
							+ "', "
							+ type.getValue()
							+ ", " + min + ", " + supplier.getId() + ");");
			connection
					.executeSQLInsertStatement("INSERT INTO `product_supplier` VALUES (LAST_INSERT_ID(), "
							+ supplier.getId() + ", '" + price + "');");
			connection
					.executeSQLInsertStatement("INSERT INTO `images` VALUES (LAST_INSERT_ID(), NULL, NULL, NULL, NULL);");
			//close the database connection
			connection.closeConnection();
		}
	}

    /**
     * Retrieves the product types
     * @return the product types as a List of ProdType
     */
	public List<ProdType> getTypes() {
		List<ProdType> types = new ArrayList<ProdType>();
		
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			ResultSet resultset = null;
			//execute a select statement
			resultset = connection
					.executeSQLSelectStatement("SELECT * FROM type");
			//check if there is a valid query result
			if (resultset != null) {
				try {
					while (resultset.next()) {
						//create the product types
						types.add(new ProdType(resultset.getInt("type_id"),
								resultset.getString("name")));
					}
				} catch (SQLException e) {
					logger.log(Level.SEVERE, e.getMessage());
				}
			}
			//close the database connection
			connection.closeConnection();
		}
		return types;
	}

    /**
     * Retrieves the product units
     * @return the product units as a List of ProdUnit
     */
	public List<ProdUnit> getUnits() {
		List<ProdUnit> units = new ArrayList<ProdUnit>();
		
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			ResultSet resultset = null;
			//execute a select statement
			resultset = connection
					.executeSQLSelectStatement("SELECT * FROM unit");
			//check if there is a valid query result
			if (resultset != null) {
				try {
					while (resultset.next()) {
						//create the product units
						units.add(new ProdUnit(resultset.getInt("unit_id"),
								resultset.getString("name")));
					}
				} catch (SQLException e) {
					logger.log(Level.SEVERE, e.getMessage());
				}
			}
			//close the database connection
			connection.closeConnection();
		}
		return units;
	}

	/**
	 * Adds a new log item
	 * @param name the name of the log item
	 * @param action the action of the log item
	 */
	public void doLog(String name, String action) {
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			//execute an insert statement
			connection
					.executeSQLInsertStatement("INSERT INTO product_manage_log (name, action) VALUES ('"
							+ name + "', '" + action + "')");
			//close the database connection
			connection.closeConnection();
		}
	}

    /**
     * Retrieves the log items
     * @param order the order to return the log items in
     * @return the log items as a List of LogItem
     */
	public List<LogItem> getLog(String order) {
		List<LogItem> log = new ArrayList<LogItem>();
		
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			ResultSet resultset = null;
			//execute a select statement
			resultset = connection
					.executeSQLSelectStatement("SELECT * FROM product_manage_log ORDER BY "
							+ order);
			//check if there is a valid query result
			if (resultset != null) {
				try {
					while (resultset.next()) {
						//create the log items
						LogItem logitem = new LogItem(
								resultset.getString("name"),
								resultset.getString("action"),
								resultset.getTimestamp("date"));
						log.add(logitem);
					}
				} catch (SQLException e) {
					logger.log(Level.SEVERE, e.getMessage());
				}
			}
			//close the database connection
			connection.closeConnection();
		}
		return log;
	}

    /**
     * Retrieves the suppliers
     * @return the suppliers a a List of Supplier
     */
	public List<Supplier> getSuppliers() {
		List<Supplier> suppliers = new ArrayList<Supplier>();
		
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			ResultSet resultset = null;
			//execute a select statement
			resultset = connection
					.executeSQLSelectStatement("SELECT * FROM supplier");
			//check if there is a valid query result
			if (resultset != null) {
				try {
					while (resultset.next()) {
						//create the suppliers
						Supplier supplier = new Supplier(
								resultset.getInt("supplier_id"),
								resultset.getString("name"),
								resultset.getString("adres"),
								resultset.getString("zipcode"),
								resultset.getString("phone"),
								resultset.getString("email"),
								resultset.getBigDecimal("shippingcost"));
						suppliers.add(supplier);
					}
				} catch (SQLException e) {
					logger.log(Level.SEVERE, e.getMessage());
				}
			}
			//close the database connection
			connection.closeConnection();
		}
		return suppliers;
	}

    /**
     * Retrieves the supplier for a product
     * @param id the produt id
     * @return the supplier as a Supplier object
     */
	public Supplier getSupplierFromProd(int id) {
		Supplier supplier = null;
		
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			ResultSet resultset = null;
			//execute a select statement
			resultset = connection
					.executeSQLSelectStatement("SELECT * FROM supplier WHERE supplier_id = (SELECT supplier FROM ingredient WHERE ingredient_id = "
							+ id + ")");
			//check if there is a valid query result
			if (resultset != null) {
				try {
					while (resultset.next()) {
						//create the supplier
						supplier = new Supplier(
								resultset.getInt("supplier_id"),
								resultset.getString("name"),
								resultset.getString("adres"),
								resultset.getString("zipcode"),
								resultset.getString("phone"),
								resultset.getString("email"),
								resultset.getBigDecimal("shippingcost"));
					}
				} catch (SQLException e) {
					logger.log(Level.SEVERE, e.getMessage());
				}
			}
			//close the database connection
			connection.closeConnection();
		}
		return supplier;
	}

    /**
     * Retrieves the suppliers per product
     * @param items the list of items
     * @return the suppliers per product as a HashMap of two Integers
     */
	public HashMap<Integer, Integer> getSupplierProduct(List<OrderItem> items) {
		HashMap<Integer, Integer> suppprod = new HashMap<Integer, Integer>();
		
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			ResultSet resultset = null;
			//execute a select statement
			resultset = connection
					.executeSQLSelectStatement("SELECT ingredient_id, supplier FROM ingredient");
			//check if there is a valid query result
			if (resultset != null) {
				try {
					while (resultset.next()) {
						for (OrderItem item : items) {
							if (item.getId() == resultset
									.getInt("ingredient_id")) {
								suppprod.put(resultset.getInt("ingredient_id"),
										resultset.getInt("supplier"));
							}
						}
					}
				} catch (SQLException e) {
					logger.log(Level.SEVERE, e.getMessage());
				}
			}
			//close the database connection
			connection.closeConnection();
		}
		return suppprod;
	}

    /**
     * Retrieves the price and shipping cost for a product
     * @param prodId the product id
     * @param supId the supplier id
     * @return the price and shipping cost as a BigDecimal array
     */
	public BigDecimal[] getPriceShippCost(int prodId, int supId) {
		BigDecimal price = new BigDecimal(0);
		BigDecimal shippcost = new BigDecimal(0);
		
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			ResultSet resultset = null;
			//execute a select stement
			resultset = connection
					.executeSQLSelectStatement("SELECT price, shippingcost FROM product_supplier, supplier WHERE product_id = "
							+ prodId
							+ " AND `product_supplier`.supplier_id = "
							+ supId + " AND `supplier`.supplier_id = " + supId);
			//check if there is a valid query result
			if (resultset != null) {
				try {
					while (resultset.next()) {
						//get the price and sbipping cost
						price = resultset.getBigDecimal("price");
						price.setScale(2, RoundingMode.HALF_UP);
						shippcost = resultset.getBigDecimal("shippingcost");
						shippcost.setScale(2, RoundingMode.HALF_UP);
					}
				} catch (SQLException e) {
					logger.log(Level.SEVERE, e.getMessage());
				}
			}
			//close the database connection
			connection.closeConnection();
		}
		BigDecimal[] all = { price, shippcost };
		return all;
	}

    /**
     * Saves an order
     * @param supplierId the supplier id
     * @param totalprice the total price of the order
     * @return the id of the new order as an int
     */
	public int saveOrder(int supplierId, BigDecimal totalprice) {
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			ResultSet resultset = null;
			//execute an insert statement
			connection
					.executeSQLInsertStatement("INSERT INTO `invoice`(invoice_id, supplier_id, totalprice) VALUES (NULL, "
							+ supplierId + ", '" + totalprice + "');");
			//execute a select statement
			resultset = connection
					.executeSQLSelectStatement("SELECT LAST_INSERT_ID() AS NewID;");
			//check if there is a valid query result
			if (resultset != null) {
				try {
					while (resultset.next()) {
						//get the id of the new order
						return resultset.getInt("NewID");
					}
				} catch (SQLException e) {
					logger.log(Level.SEVERE, e.getMessage());
				}
			}
			//close the database connection
			connection.closeConnection();
		}
		return 0;
	}

	/**
	 * Saves an item for an order
	 * @param prodId the product id
	 * @param number the product amount
	 * @param newId the new invoice id
	 */
	public void saveOrderItem(int prodId, int number, int newId) {
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			//execute an insert statement
			connection
					.executeSQLInsertStatement("INSERT INTO `invoice_product`(product_id, invoice_id, number) VALUES ("
							+ prodId + ", " + newId + ", " + number + ")");
			//close the database connection
			connection.closeConnection();
		}
	}

    /**
     * Retrieves the orders
     * @return the orders as a List of Order
     */
	public List<Order> getOrders() {
		List<Order> orders = new ArrayList<Order>();
		
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			ResultSet resultset = null;
			//execute a select statement
			resultset = connection
					.executeSQLSelectStatement("SELECT * FROM `invoice`");
			List<Supplier> suppliers = new ArrayList<Supplier>();
			//check if there is a valid query result
			if (resultset != null) {
				try {
					while (resultset.next()) {
						Supplier newSupplier = null;
						//add the suppliers
						for (Supplier supplier : suppliers) {
							if (supplier.getId() == resultset
									.getInt("supplier_id")) {
								newSupplier = supplier;
							}
						}
						//create the order
						Order order = new Order(newSupplier);
						order.setId(resultset.getInt("invoice_id"));
						order.setTotalPrice(resultset
								.getBigDecimal("totalprice"));
						order.setDate(resultset.getTimestamp("date"));
						order.setState(resultset.getString("state"));
						orders.add(order);
					}
				} catch (SQLException e) {
					logger.log(Level.SEVERE, e.getMessage());
				}
			}
			//close the database connection
			connection.closeConnection();
		}
		return orders;
	}

	/**
	 * Retrieves the order items for an order
	 * @param id the order id
	 * @return the items as a List of OrderItem
	 */
	public List<OrderItem> getOrderItemsByOrder(int id) {
		List<OrderItem> orderItems = new ArrayList<OrderItem>();
		
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			ResultSet resultset = null;
			//execute a select statement
			resultset = connection
					.executeSQLSelectStatement("SELECT * FROM viewinvoices WHERE invoice_id = "
							+ id);
			//check if there is a valid query result
			if (resultset != null) {
				try {
					while (resultset.next()) {
						//create the order items
						OrderItem orderItem = new OrderItem(
								resultset.getInt("product_id"),
								resultset.getString("name"), null,
								resultset.getInt("number"),
								resultset.getBigDecimal("price"));
						orderItems.add(orderItem);
					}
				} catch (SQLException e) {
					logger.log(Level.SEVERE, e.getMessage());
				}
			}
			//close the database connection
			connection.closeConnection();
		}
		return orderItems;
	}

	/**
	 * Retrieves the supplier for an order
	 * @param id the order id
	 * @return the supplier as a Supplier object
	 */
	public Supplier getSupplierByOrder(int id) {
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			ResultSet resultset = null;
			//execute a select statement
			resultset = connection
					.executeSQLSelectStatement("SELECT * FROM `supplier` WHERE supplier_id = (SELECT supplier_id FROM `invoice` WHERE invoice_id = "
							+ id + ")");
			//check if there is a valid query result
			if (resultset != null) {
				try {
					while (resultset.next()) {
						return new Supplier(resultset.getInt("supplier_id"),
								resultset.getString("name"),
								resultset.getString("adres"),
								resultset.getString("zipcode"),
								resultset.getString("phone"),
								resultset.getString("email"),
								resultset.getBigDecimal("shippingcost"));
					}
				} catch (SQLException e) {
					logger.log(Level.SEVERE, e.getMessage());
				}
			}
			//close the database connection
			connection.closeConnection();
		}
		return null;
	}

    /**
     * Saves a supplier
     * @param supplier the supplier
     */
	public void saveSupplier(Supplier supplier) {
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			//execute an update statement
			connection
					.executeSQLUpdateStatement("UPDATE `supplier` SET name = '"
							+ supplier.getName() + "', adres = '"
							+ supplier.getAddress() + "', zipcode = '"
							+ supplier.getZipcode() + "', phone = '"
							+ supplier.getPhone() + "', email = '"
							+ supplier.getEmail() + "', shippingcost = '"
							+ supplier.getShippingCost()
							+ "' WHERE supplier_id = " + supplier.getId());
			//close the database connection
			connection.closeConnection();
		}

	}

    /**
     * Adds a supplier
     * @param supplier the supplier
     */
	public void addSupplier(Supplier supplier) {
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			//execute an insert statement
			connection
					.executeSQLInsertStatement("INSERT INTO `supplier` VALUES (NULL, '"
							+ supplier.getName()
							+ "', '"
							+ supplier.getAddress()
							+ "', '"
							+ supplier.getZipcode()
							+ "', '"
							+ supplier.getPhone()
							+ "', '"
							+ supplier.getEmail()
							+ "', '"
							+ supplier.getShippingCost() + "')");
			//close the database connection
			connection.closeConnection();
		}
	}

    /**
     * Adds a product unit
     * @param unit the product unit
     */
	public void addUnit(ProdUnit unit) {
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			//execute an insert statement
			connection
					.executeSQLInsertStatement("INSERT INTO `unit` VALUES (NULL, '"
							+ unit.getName() + "')");
			//close the database connection
			connection.closeConnection();
		}
	}

    /**
     * Adds a product type
     * @param type the product type
     */
	public void addType(ProdType type) {
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			//execute an insert statement
			connection
					.executeSQLInsertStatement("INSERT INTO `type` VALUES (NULL, '"
							+ type.getName() + "')");
			//close the database connection
			connection.closeConnection();
		}
	}

	/**
	 * Retrieves an image
	 * @param id the image id
	 * @return the image as an ImageIcon object
	 */
	public ImageIcon getImage(int id) {
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			ResultSet resultSet = null;
			//execute a select statement
			resultSet = connection
					.executeSQLSelectStatement("SELECT Image FROM `images` WHERE image_id = "
							+ id);
			//check if there is a valid query result
			if (resultSet != null) {
				try {
					while (resultSet.next()) {
						//create the image
						byte[] bytes = resultSet.getBytes("image");
						if (bytes != null) {
							return new ImageIcon(bytes);
						}
					}
				} catch (SQLException e) {
					logger.log(Level.SEVERE, e.getMessage());
				}
			}
			//close the database connection
			connection.closeConnection();
		}
		return null;
	}

    /**
     * Adds an image
     * @param file the image file
     * @return the new image id as an int
     */
	public int setImage(File file) {
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			try {
				FileInputStream fis = new FileInputStream(file);
				//add the image
				connection.executeAddImage("image", fis, (int) file.length(),
						"" + file.length(), file.getName());
				ResultSet resultSet = null;
				//execute a select statement
				resultSet = connection
						.executeSQLSelectStatement("SELECT LAST_INSERT_ID() AS NewID");
				//check if there is a valid query result
				if (resultSet != null) {
					try {
						while (resultSet.next()) {
							return resultSet.getInt("NewID");
						}
					} catch (SQLException e) {
						logger.log(Level.SEVERE, e.getMessage());
					}
				}
				//close the database connection
				connection.closeConnection();
			} catch (FileNotFoundException e) {
				logger.log(Level.SEVERE, e.getMessage());
			}
		}
		return 0;
	}

    /**
     * Updates an image
     * @param file the image file
     * @param id the image id
     */
	public void updateImage(File file, int id) {
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection() && file != null) {
			try {
				FileInputStream fis = new FileInputStream(file);
				//update the image
				connection.executeUpdateImage("image", fis,
						(int) file.length(), "" + file.length(),
						file.getName(), id);
				//close the database connection
				connection.closeConnection();
			} catch (FileNotFoundException e) {
				logger.log(Level.SEVERE, e.getMessage());
			}
		}
	}

    /**
     * Updates the price of a product
     * @param price the new product price
     * @param prodId the product id
     */
	public void updatePrice(BigDecimal price, int prodId) {
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			//execute an update statement
			connection
					.executeSQLUpdateStatement("UPDATE `product_supplier` SET price = '"
							+ price + "' WHERE product_id = " + prodId);
			//close the database connection
			connection.closeConnection();
		}
	}

	/**
	 * Retrieves the image id for a product
	 * @param id the product id
	 * @return the image id as an int
	 */
	public int getImageId(int id) {
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			ResultSet resultSet = null;
			//execute a select statement
			resultSet = connection
					.executeSQLSelectStatement("SELECT image FROM `ingredient` WHERE ingredient_id = "
							+ id);
			if (resultSet != null) {
				try {
					while (resultSet.next()) {
						return resultSet.getInt("image");
					}
				} catch (SQLException e) {
					logger.log(Level.SEVERE, e.getMessage());
				}
			}
			//close the database connection
			connection.closeConnection();
		}
		return 0;
	}

    /**
     * Changes the state of an order
     * @param id the order id
     */
	public void changeState(int id) {
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			//execute an update statement
			connection
					.executeSQLUpdateStatement("UPDATE `invoice` SET state = 'Geleverd' WHERE invoice_id = "
							+ id);
			//close the database connection
			connection.closeConnection();
		}
	}

    /**
     * Updates the stock of a product
     * @param user the user performing the action
     * @param id the product id
     * @param amount the new stock amount
     * @return the result as a boolean
     */
	public boolean updateAmount(String user, int id, int amount) {
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			ResultSet resultSet = null;
			//execute a select statement
			resultSet = connection
					.executeSQLSelectStatement("SELECT name, stock FROM `ingredient` WHERE ingredient_id = "
							+ id);
			try {
				if (resultSet.next()) {
					if (resultSet.getInt("stock") + amount < 0) {
						return false;
					} else {
						//log the update action
						doLog(user, "Update product amount: "+resultSet.getString("name")+" "+amount);
						//execute an update statement
						connection
								.executeSQLUpdateStatement("UPDATE `ingredient` SET stock = "
										+ resultSet.getInt("stock")
										+ " + "
										+ amount
										+ " WHERE ingredient_id = "
										+ id);
					}
				}
			} catch (SQLException e) {
				logger.log(Level.SEVERE, e.getMessage());
			}
			//close the database connection
			connection.closeConnection();
		}
		return true;
	}

    /**
     * Deletes an orde
     * @param id the order id
     */
	public void deleteOrder(int id) {
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		//set the database user to the manager
		connection.setDBUser("MagazijnchefC4");
		if (connection.openConnection()) {
			//execute a delete statement
			connection
					.executeSQLDeleteStatement("DELETE FROM `invoice` WHERE invoice_id = "
							+ id);
			//close the database connection
			connection.closeConnection();
		}
	}

    /**
     * Truncates the log
     */
	public void truncateLog() {
		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		//set the database user to the manager
		connection.setDBUser("MagazijnchefC4");
		if (connection.openConnection()) {
			//execute an update statement
			connection.executeSQLUpdateStatement("TRUNCATE product_manage_log");
			//close the database connection
			connection.closeConnection();
		}
	}
}