package datastorage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginDAO {
	
	//The logger instance
	private Logger logger = Logger.getLogger(LoginDAO.class.getName());

    /**
     * Checks whether an employee login is valid
     * @param barcode the barcode for the employee
     * @param manager a flag to determine whether an employee is a manager
     * @return the login result as a String, either the employee name or empty
     */
	public String checkLogin(int barcode, boolean manager) {
		String result = "";

		//create and open a new database connection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			//execute a select statement based on the manager flag
			ResultSet resultset;
			if (manager) {
				resultset = connection
						.executeSQLSelectStatement("SELECT firstname, lastname FROM `employee` WHERE barcode = "
								+ barcode + " AND (fk_position_id = 4 OR fk_position_id = 5)");
			} else {
				resultset = connection
						.executeSQLSelectStatement("SELECT firstname, lastname FROM `employee` WHERE barcode = "
								+ barcode + " AND (fk_position_id = 4)");
			}
			try {
				//check if there is a valid query result
				if (resultset.next()) {
					result = resultset.getString("firstname") + " "
							+ resultset.getString("lastname");
				}
			} catch (SQLException e) {
				logger.log(Level.SEVERE, e.getMessage());
			}
			//close the database connection
			connection.closeConnection();
		}
		return result;
	}
}