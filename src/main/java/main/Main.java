package main;

import businesslogic.ProductManager;
import presentation.ManageGUI;

public class Main {

    /**
     * The main method
     * @param args the application arguments
     */
	public static void main(String[] args) {
		//create the product manager
		ProductManager manager = new ProductManager();
		
		//create the manage GUI, the main screen of our application
		new ManageGUI(manager);
	}
}