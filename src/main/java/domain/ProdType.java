package domain;

public class ProdType {
	
	//The value of this product type
	private int value;
	
	//The name of this product type
	private String name;

    /**
     * Initializes the fields of this class
     * @param value the id of this product type
     * @param name the name of this product type
     */
	public ProdType(int value, String name) {
		this.value = value;
		this.name = name;
	}

    /**
     * Accessor method to get the value of this product type
     * @return the value as an int
     */
	public int getValue() {
		return value;
	}

    /**
     * Accessor method to get the name of this product type
     * @return the name as a String
     */
	public String getName() {
		return name;
	}
}
