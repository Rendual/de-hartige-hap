package domain;

import java.sql.Timestamp;

public class LogItem {
	
	//The user for this log item
	private String user;
	
	//The description of this log item
	private String description;
	
	//The timestamp for this log item
	private Timestamp time;

    /**
     * Initializes the fields of this class
     * @param user the user
     * @param description the description
     * @param time the time this item was created
     */
	public LogItem(String user, String description, Timestamp time) {
		this.user = user;
		this.description = description;
		this.time = time;
	}

    /**
     * Accessor method to get the user for this log item
     * @return the user as String
     */
	public String getUser() {
		return user;
	}

    /**
     * Accessor method to get the description of this log item
     * @return the description as String
     */
	public String getDescription() {
		return description;
	}

    /**
     * Accessor method to get the timestamp for this log item
     * @return the timestamp as Timestamp
     */
	public Timestamp getTime() {
		return time;
	}
}