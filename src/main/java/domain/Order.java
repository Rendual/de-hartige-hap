package domain;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class Order {
	
	//The id of this order
	private int id;
	
	//Tje supplier of this order
	private Supplier supplier;
	
	//The total price of this order
	private BigDecimal totalPrice;
	
	//The list of items of this order
	private List<OrderItem> orderItems;
	
	//The date of this order
	private Timestamp date;
	
	//The state of this order
	private String state;
	
    /**
     * Initializes the fields of this class
     * @param supplier the supplier
     */
	public Order(Supplier supplier) {
		this.supplier = supplier;
		totalPrice = new BigDecimal(0);
		orderItems = new ArrayList<OrderItem>();
	}

	/**
	 * Calculates the total price of this order
	 */
	public void calculateTotalPrice() {
		for(OrderItem orderItem : orderItems) {
			//multiply the price of an order item by the amount of this item
			BigDecimal ordertotal = orderItem.getPrice().multiply(new BigDecimal(""+orderItem.getNum()));
			
			//add the calculated total value of the amount of an item to the total price
			totalPrice = totalPrice.add(ordertotal);
		}
	}
	
    /**
     * Accessor method to get the id of this order
     * @return the order id as an int
     */
	public int getId() {
		return id;
	}

    /**
     * Set the id for this order
     * @param id the new id
     */
	public void setId(int id) {
		this.id = id;
	}

    /**
     * Accessor method to get the supplier of this order
     * @return the supplier as a Supplier object
     */
	public Supplier getSupplier() {
		return supplier;
	}
	
    /**
     * Set the supplier for this order
     * @param supplier the new supplier
     */
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

    /**
     * Accessor method to get the total price of this order
     * @return the total price as BigDecimal
     */
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	
    /**
     * Set the total price for this order
     * @param totalPrice the new total price
     */
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	
    /**
     * Adds an item to this order
     * @param item the order item
     */
	public void addOrderItem(OrderItem item) {
		orderItems.add(item);
	}

    /**
     * Accessor method to get the items for this order
     * @return the list of orders as a List object
     */
	public List<OrderItem> getOrderItems() {
		return orderItems;
	}

    /**
     * Accessor method to get the date of this order
     * @return the date as Timestamp
     */
	public Timestamp getDate() {
		return date;
	}

    /**
     * Set the date for this order
     * @param date the new date
     */
	public void setDate(Timestamp date) {
		this.date = date;
	}

    /**
     * Accessor method to get the state of this order
     * @return the state as String
     */
	public String getState() {
		return state;
	}

    /**
     * Set the state for this order
     * @param status the new order status
     */
	public void setState(String status) {
		this.state = status;
	}
}