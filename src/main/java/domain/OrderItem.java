package domain;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class OrderItem {

	//The id of this order item
	private int id;
	
	//The name of this order item
	private String name;
	
	//The supplier of this order item
	private Supplier supplier;
	
	//The amount of this order item
	private int num;
	
	//The price of this order item
	private BigDecimal price;

    /**
     * Initializes the fields of this class
     * @param id the id of this order item
     * @param name the name of this order item
     * @param supplier the supplier of this order item
     * @param num the amount of this order item
     * @param price the price of this order item
     */
	public OrderItem(int id, String name, Supplier supplier, int num, BigDecimal price) {
		this.id = id;
		this.name = name;
		this.supplier = supplier;
		this.num = num;
		this.price = price.setScale(2, RoundingMode.HALF_UP);
	}

    /**
     * Accessor method to get the id of this order item
     * @return the order item id as an int
     */
	public int getId() {
		return id;
	}

    /**
     * Accessor method to get the name of this order item
     * @return the order item name as a String
     */
	public String getName() {
		return name;
	}

    /**
     * Accessor method to get the supplier of this order item
     * @return the order item supplier as a Supplier object
     */
	public Supplier getSupplier() {
		return supplier;
	}

    /**
     * Accessor method to get the amount of this order item
     * @return the order item amount as an int
     */
	public int getNum() {
		return num;
	}
	
    /**
     * Set the amount for this order item
     * @param num the new amount
     */
	public void setNum(int num) {
		this.num = num;
	}

    /**
     * Accessor method to get the price of this order item
     * @return the order item price as a BigDecimal
     */
	public BigDecimal getPrice() {
		return price;
	}
}