package domain;

import java.math.BigDecimal;

public class Supplier {

	//The id of this supplier
	private int id;
	
	//The name of this supplier
	private String name;
	
	//The address of this supplier
	private String address;
	
	//The zipcode of this supplier
	private String zipcode;
	
	//The phonenumber of this supplier
	private String phone;
	
	//The email of this supplier
	private String email;
	
	//The shipping cost for this supplier
	private BigDecimal shippingCost;

    /**
     * Initializes the fields of this class
     * @param id the id of this supplier
     * @param name the name of this supplier
     * @param address the address of this supplier
     * @param zipcode the zipcode of this supplier
     * @param phone the phonenumer of this supplier
     * @param email the email of this aupplier
     * @param shippingCost the shipping cost for this supplier
     */
	public Supplier(int id, String name, String address, String zipcode,
			String phone, String email, BigDecimal shippingCost) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.zipcode = zipcode;
		this.phone = phone;
		this.email = email;
		this.shippingCost = shippingCost;
	}

    /**
     * Accessor method to get the id of this supplier
     * @return the supplier id as an int
     */
	public int getId() {
		return id;
	}

    /**
     * Accessor method to get the name of this supplier
     * @return the name as a String
     */
	public String getName() {
		return name;
	}

    /**
     * Accessor method to get the address of this supplier
     * @return the address as a String
     */
	public String getAddress() {
		return address;
	}

    /**
     * Accessor method to get the zipcode of this supplier
     * @return the zipcode as a String
     */
	public String getZipcode() {
		return zipcode;
	}

    /**
     * Accessor method to get the phonenumber of this supplier
     * @return the phonenumber as a String
     */
	public String getPhone() {
		return phone;
	}

    /**
     * Accessor method to get the email of this supplier
     * @return the email as a String
     */
	public String getEmail() {
		return email;
	}

    /**
     * Accessor method to get the shipping cost for this supplier
     * @return the shipping cost as a BigDecimal
     */
	public BigDecimal getShippingCost() {
		return shippingCost;
	}
}