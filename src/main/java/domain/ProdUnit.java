package domain;

public class ProdUnit {

	//The id of this product unit
	private int id;
	
	//The name of this product unit
	private String name;

    /**
     * Initializes the fields of this class
     * @param id the id of this product unit
     * @param name the name of this product unit
     */
	public ProdUnit(int id, String name) {
		this.id = id;
		this.name = name;
	}

    /**
     * Accessor method to get the id of this product unit
     * @return the product unit id as an int
     */
	public int getId() {
		return id;
	}

    /**
     * Accessor method to get the name of this product unit
     * @return the product unit name as a String
     */
	public String getName() {
		return name;
	}
}
