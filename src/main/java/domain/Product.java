package domain;

import javax.swing.ImageIcon;

public class Product {

	//The id of this product
	private int id;
	
	//The title of this product
	private String title;
	
	//The description of this product
	private String description;
	
	//The image of this product
	private ImageIcon image;
	
	//The current stock of this product
	private int stock;
	
	//The product unit of this product
	private ProdUnit unit;
	
	//The minimum stock of this product
	private int min;
	
	//The product type of this product
	private ProdType type;
	
	//The supplier of this product
	private Supplier supplier;

    /**
     * Initializes the fields of this class
     * @param id the id of this product
     * @param title the title of this product
     * @param description the description of this product
     * @param image the image of this product
     * @param stock the current stock of this product
     * @param unit the product unit of this product
     * @param min the minimum stock of this product
     * @param type the product type of this product
     * @param supplier the supplier of this product
     */
	public Product(int id, String title, String description, ImageIcon image,
			int stock, ProdUnit unit, int min, ProdType type, Supplier supplier) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.image = image;
		this.stock = stock;
		this.unit = unit;
		this.min = min;
		this.type = type;
		this.supplier = supplier;
	}

    /**
     * Accessor method to get the supplier of this product
     * @return the supplier as a Supplier object
     */
	public Supplier getSupplier() {
		return supplier;
	}

    /**
     * Accessor method to get the id of this product
     * @return the id as an int
     */
	public int getId() {
		return id;
	}

    /**
     * Set the id for this product
     * @param id the new id
     */
	public void setId(int id) {
		this.id = id;
	}

    /**
     * Accessor method to get the title of this product
     * @return the title as a String
     */
	public String getTitle() {
		return title;
	}

    /**
     * Set the title for this product
     * @param title the new title
     */
	public void setTitle(String title) {
		this.title = title;
	}

    /**
     * Accessor method to get the description of this product
     * @return the description as a String
     */
	public String getDescription() {
		return description;
	}

    /**
     * Set the description for this product
     * @param description the new description
     */
	public void setDescription(String description) {
		this.description = description;
	}

    /**
     * Accessor method to get the image of this product
     * @return the supplier as an ImageIcon
     */
	public ImageIcon getImage() {
		return image;
	}

    /**
     * Set the image for this product
     * @param image the new image
     */
	public void setImage(ImageIcon image) {
		this.image = image;
	}

    /**
     * Accessor method to get the current stock of this product
     * @return the current stock as an int
     */
	public int getStock() {
		return stock;
	}

    /**
     * Set the current stock for this product
     * @param stock the stock
     */
	public void setStock(int stock) {
		this.stock = stock;
	}

    /**
     * Accessor method to get the type of this product
     * @return the supplier as a ProdType object
     */
	public ProdType getType() {
		return type;
	}

    /**
     * Accessor method to get the minimum stock of this product
     * @return the minimum stock as an int
     */
	public int getMin() {
		return min;
	}

    /**
     * Accessor method to get the unit of this product
     * @return the supplier as a Supplier object
     */
	public ProdUnit getUnit() {
		return unit;
	}
}